<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Filename: content--background-images.php
*
*/

// Theme Vars
$template_dir = Daymarker( 'template_dir' );
$carousel_icon_nav = $template_dir . "/img/ui/BARBER-web-content-ui-icon-carousel-nav-next-beige.svg";

// Default Vars
$images = $images_count = $is_gallery = false;
if ( get_sub_field( 'images' ) ) {
	$images = get_sub_field( 'images' );
	$images_count = count( $images );
	if ( $images_count > 1 ) {
		$is_gallery = true;
	}
}

if ( $images_count ) {
		
	echo '<div class="carousel-container carousel-container--front-page" id="front-page-carousel--0' . $row_count . '">';
	if ( $is_gallery ) {
		echo '<div class="owl-carousel js--vpa-match-viewport-height">';
	}
	
	while ( have_rows( 'images' ) ) {
		
		// setup row
		the_row();
		
		// images vars
		$image = $image_src = false;
		if ( get_sub_field( 'image' ) ) {
			$image = get_sub_field( 'image' );
			$image_src = $image['url'];
		}
		
		// message vars
		$message = $cta = $link = $colour_theme = $pos_hor = $post_vert = false;
		$message_obj = array();
		$message_obj['colour_theme'] = get_sub_field( 'colour_theme' );
		$message_obj['message'] = get_sub_field( 'message' );
		$message_obj['cta'] = get_sub_field( 'cta' );
		$message_obj['link'] = get_sub_field( 'link' );
		$message_obj['pos_hor'] = get_sub_field( 'pos_hor' );
		$message_obj['pos_vert'] = get_sub_field( 'pos_vert' );
		
		echo '<div class="background-image lazyload-container lazyload-container--background-image js--vpa-match-viewport-height">';
		
		if ( $image_src ) {
			echo '<div class="lazyload lazyload--background-image" data-bg="' . $image_src . '"></div>';
		}
		
		include( locate_template( './includes/front-page/content--message.php' ) );		
		
		echo '</div>';
		echo '<!-- /.background-image -->';
			
	}	
	
	if ( $is_gallery ) {
			
		echo '</div>';
		echo '<!-- /.owl-carousel -->';
		
		echo '<div class="carousel-controls carousel-controls--prev prev"><img src="' . $carousel_icon_nav . '" alt="Previous Carousel Item"/></div>';
		echo '<div class="carousel-controls carousel-controls--next next"><img src="' . $carousel_icon_nav . '" alt="Next Carousel Item"/></div>';

	}		

	echo '</div>';
	echo '<!-- /.carousel-container -->';	
		
		
} else {
	echo '<!-- No Image(s) -->';
}
	
?>