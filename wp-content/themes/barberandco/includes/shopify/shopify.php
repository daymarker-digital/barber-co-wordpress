<?php 

function Shopify_getCollection( $storeURL = null, $colletionID = null, $storeAuth = null, $productLimit = null ) {
	
	////////////////////////////////////
	//// Instructions
	////////////////////////////////////
	
	// Defaults
	
	$collectionURL = "";
	$featureProducts = array();
	
	if ( $storeURL === null || $colletionID === null || $storeAuth === null ) :
	
		// Parameters not set!
		// Exit!
		return;
		
	else :
	
		// Parameters set!
		// Do Stuff!
		
		// Build Endpoint
		$collectionURL = $storeAuth . '/admin/collects.json?collection_id=' . $colletionID;
		if ( $productLimit !== null ) :
			$collectionURL .= '&limit=' . $productLimit . '';
		endif;
		
		// Get Results
		$collection = @file_get_contents( $collectionURL );
		
		if ( $collection ) : 
		
			// Results Received, Yay!
			$collectionJSON = json_decode( $collection, true );
			$collectionJSON = $collectionJSON['collects'];
			
			// Get Count of Total Products in Collection
			$collectionCount = count( $collectionJSON );
			
			// Loop Through Collection of Products
			for ( $i = 0; $i < $collectionCount; $i++ ) :
											
				// Shopify API Product Endpoint 
				// admin/products/####.json
											
				$productID = $collectionJSON[$i]['product_id'];
				$productURL = $storeAuth . '/admin/products/' . $productID . '.json';
				$product = @file_get_contents( $productURL );
				
				if ( $product ) : 
					
					// Product Data from Endpoint Exitst!
					// Convert String to JSON
					$productJSON = json_decode( $product, true );
					// Add Product to 'Featured Products' Array
					array_push( $featureProducts, $productJSON );
					
				else:
					// No Product Data from Endpoint, Go to Next Product
					continue;
				endif;
				
			endfor;
			
			// Print Featured Products!			
			Shopify_printCollection( $featureProducts, $storeURL );
			
		else : 
			// No Collection Data from Endpoint : (
			return;
		endif; 
			
	endif;
		
} // Shopify_getCollection()

function Shopify_printCollection( $arrayOfProducts = null, $storeURL = null ) {
	
	if ( $arrayOfProducts !== null && !empty( $arrayOfProducts ) && $storeURL !== null ) :
	
		// Get Count of Total Products in Collection
		$collectionCount = count( $arrayOfProducts );
		
		// Loop Through Collection of Products
		for ( $i = 0; $i < $collectionCount; $i++ ) :
		
			$product = $arrayOfProducts[$i]['product'];
			$productTitle = $product['title'];
			$productLink = $storeURL . '/products/'. $product['handle'];
					
			echo '<li>';
			echo '<a href="' . $productLink . '" target="_blank">';
			echo '<h2>' . $productTitle . '</h2>';
			echo '</a>';
			echo '</li>';
		
		endfor;
	
	else: 
	
		// $arrayOfProducts is empty or equal to 'null' :(
		return;
	
	endif;
	
}
	
?>