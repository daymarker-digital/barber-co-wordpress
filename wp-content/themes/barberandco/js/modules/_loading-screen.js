//////////////////////////////////////////////////////////
////  Loading Screen
//////////////////////////////////////////////////////////

var LoadingScreen = (function () {

	var debugThis = false;
	var info = { name : 'LoadingScreen', version : 1.0 };
	var loadingScreenEl = $(".js-loading-screen--page-load");
		
	//////////////////////////////////////////////////////////
	//// Private
	//////////////////////////////////////////////////////////

	var _hideScreen = function () {
					
		if ( loadingScreenEl.length ) {
				
			loadingScreenEl.addClass("loaded");
			
			loadingScreenEl.one("webkitTransitionEnd otransitionend oTransitionEnd MSTransitionEnd transitionend", function(event) {
				$("body").addClass("loading-complete");
				loadingScreenEl.css({
					'z-index' : '-999999999'
				});
			});
				
		} // end if
		
	};
		
	//////////////////////////////////////////////////////////
	//// Public
	//////////////////////////////////////////////////////////
		
	var init = function() {
	
		Debug.log( debugThis, info, 'start' );
	
		_hideScreen();
		
		Debug.log( debugThis, info, 'stop' );
		
	};
	
	//////////////////////////////////////////////////////////
	////  Returned
	//////////////////////////////////////////////////////////
	
	return {
		init : init
	};

})();