<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Filename: common--loading-screen.php
*
*/

// Theme Vars
$home = Daymarker( 'home_url' );
$shopify_url = Daymarker( 'shopify_url' );
$template_dir = Daymarker( 'template_dir' );
$is_production = Daymarker( 'production' );
$is_maintenance = Daymarker( 'maintenance' );
$theme_classes = Daymarker( 'theme_classes' );

?>

<div class="loading-screen loading-screen--page-load js-loading-screen--page-load">
	
	<div class="loader loader--brand">
		<div class="brand">
			<img src="<?php echo $template_dir; ?>/img/brand/BARBER-web-content-logo-symbol-beige.svg" alt="Barber & Co. Logo"/>
		</div>
		<!-- /.brand -->
	</div>
	<!-- /.loader -->

</div>
<!-- /.loading-screen--page-load -->

<div class="loading-screen loading-screen--busy js-loading-screen--busy">
	
	<div class="loader loader--spinner">
		<div class="spinner"></div>
	</div>
	
</div>
<!-- /.loading-screen--busy -->