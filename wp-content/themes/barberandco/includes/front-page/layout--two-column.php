<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Filename: layout--two-column.php
*
*/

// Theme Vars
$home = Daymarker( 'home_url' );
$shopify_url = Daymarker( 'shopify_url' );
$template_dir = Daymarker( 'template_dir' );
$is_production = Daymarker( 'production' );
$is_maintenance = Daymarker( 'maintenance' );
$theme_classes = Daymarker( 'theme_classes' );

$carousel_icon_nav = $template_dir . "/img/ui/BARBER-web-content-ui-icon-carousel-nav-next-beige.svg";

if ( have_rows( 'content' ) ) {
	
	$block_count = 1;
	
	echo '<div class="carousel-container carousel-container--front-page carousel-container--split" id="front-page-split-carousel--0' . $row_count . '">';
	echo '<div class="owl-carousel">';
	
	while ( have_rows( 'content' ) ) {
		
		// Setup Row
		the_row();
		
		// Row Vars
		$content_type = get_sub_field( 'content_type' );

		// Block Classes
		$block_classes = '';
		$block_classes .= ' block--0' . $block_count;
		$block_classes .= ' block--' . $content_type;
				
		// Print Row
		echo '<div class="block' . $block_classes . '">';
		
		switch ( $content_type ) {
			case 'background-image':
				include( locate_template( './includes/front-page/content--background-image.php' ) );
				break;
			case 'inline-image':
				include( locate_template( './includes/front-page/content--inline-image.php' ) );
				break;
			case 'background-colour':
				include( locate_template( './includes/front-page/content--background-colour.php' ) );
				break;
		}
		
		echo '</div>';
		
		$block_count++;
		
	}
	
	echo '</div>';
	echo '<!-- /.owl-carousel -->';
	
	echo '<div class="carousel-controls carousel-controls--prev prev"><img src="' . $carousel_icon_nav . '" alt="Previous Carousel Item"/></div>';
	echo '<div class="carousel-controls carousel-controls--next next"><img src="' . $carousel_icon_nav . '" alt="Next Carousel Item"/></div>';
		
	echo '</div>';
	echo '<!-- /.carousel-container--front-page-split -->';
	
} else {
	echo '<!-- No Content -->';
}

?>