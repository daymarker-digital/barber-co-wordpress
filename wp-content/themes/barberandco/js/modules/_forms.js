//////////////////////////////////////////////////////////
////  Forms
//////////////////////////////////////////////////////////

var Forms = (function () {

	var debugThis = false;
  	var info = { name : 'Forms', version : 1.1 };
  	var busyScreenEl = $(".js-loading-screen--busy");
  	var formValidationObj = {
		'submitValue' : 'Submit',
		'dateFormat' : 'YYYY-MM-DD',
		'fileTypes' : [ 'pdf', 'jpg', 'gif', 'png' ],
		'regexValues' : {
			'phone' : /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/,
			'email' : /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i
		},
		'errorMessages' : {
			"text" : "Oops! This field cannot be blank.",
			"number" : "Please enter a valid number!",
			"textarea" : "Oops! This field cannot be blank.",
			"phone" : "Please enter a valid phone number!",
			"email" : "Please enter a valid email address!",
			"radio" : "Please select a choice!",
			"checkbox" : {
				"general" : "Please select at least one choice!",
				"terms" : "This field must be 'checked'"
			},
			"file" : "Please upload a valid file format: PDFs, JPGs and PNGs only!",
			"date" : "Please enter a valid date using the following format: YYYY-MM-DD",
			"general" : "There was a problem with your submission. Errors have been highlighted above."
		}
	};
	
	//////////////////////////////////////////////////////////
	////  Private Methods
	//////////////////////////////////////////////////////////
		
	var _setupForm = function ( $formContainerElement ) {
		
		if ( !_.isEmpty( $formContainerElement ) ) {
	      	
	    	var formEl;
			var errorMessage = "";
			var submitValue = formValidationObj.submitValue;
			var formValidationMessage = formValidationObj.errorMessages;
			var $formData;
				
			// check for custom submit button
			if ( $formContainerElement.attr( "data-info" ) ) {
				$formData = $formContainerElement.data("info");
				if ( $formData.altSubmit.length ) {
					submitValue = $formData.altSubmit; 
				}
			}
		
			// set form reference var
			formEl = $formContainerElement.find("form");
						
			// set set inputs to 'read-only'													
			formEl.find(".read-only").each(function(){
				$(this).find("input").prop( 'readonly', true );
			});
		
			// iterate through each required field, setting custom error message			
			formEl.find( ".required-field" ).each( function(){
				
				var thisRequiredField = $(this);
				
				if ( thisRequiredField.hasClass( "text" ) ) { 
					errorMessage = formValidationMessage.text;					
				}
				
				if ( thisRequiredField.hasClass( "number" ) ) { 
					errorMessage = formValidationMessage.number;					
				}
											
				if ( thisRequiredField.hasClass( "textarea" ) ) { 
					errorMessage = formValidationMessage.textarea;					
				}
							
				if ( thisRequiredField.hasClass( "phone" ) ) { 
					errorMessage = formValidationMessage.phone;					
				}
							
				if ( thisRequiredField.hasClass( "email" ) ) { 
					errorMessage = formValidationMessage.email;					
				}
							
				if ( thisRequiredField.hasClass( "file" ) ) { 
					errorMessage = formValidationMessage.file;					
				}
							
				if ( thisRequiredField.hasClass( "date" ) ) { 
					errorMessage = formValidationMessage.date;					
				}
							
				if ( thisRequiredField.hasClass( "radio" ) ) {
					errorMessage = formValidationMessage.radio;				
				}
							
				if ( thisRequiredField.hasClass( "checkbox" ) ) { 
								
					if ( thisRequiredField.hasClass( "terms" ) ) {
						errorMessage = formValidationMessage.checkbox.terms;	
					} else {
						errorMessage = formValidationMessage.checkbox.general;	
					}
												
				}
	
				thisRequiredField.append("<div class='error-message'><p>" + errorMessage + "</p></div>");
												
			}); // each '.required-field'
					
			// set custom 'submit' button			
			$( "<fieldset class='col-xs-12 submit'><input type='submit' value='" + submitValue + "'><div class='error-message error-message--general'><p>" + formValidationMessage.general + "<p></div></fieldset>" ).appendTo( formEl );
	      	
    	}  
    	     
	};
	
	var _validateField = function ( $requiredField ) {
		
		if ( !_.isEmpty( $requiredField ) ) {
		
			var isValidField = false;
			var inputValue = "";
			var inputLabel = "";
			var inputType = "";
					
			inputLabel = $requiredField.find( "label" ).text();
				
			// validate 'text' field	
			if ( $requiredField.hasClass( "text" ) ) {
				inputType = "text";
				inputValue = $requiredField.find( "input" ).val();
				if ( inputValue.length ) {
					isValidField = true;
				}
			} 
			
			// validate 'number' field	
			if ( $requiredField.hasClass( "number" ) ) {
				inputType = "number";
				inputValue = $requiredField.find( "input" ).val();
				if ( $.isNumeric( inputValue ) ) {
					isValidField = true;
				}
			} 
			
			// validate 'textarea' field
			if ( $requiredField.hasClass( "textarea" ) ) {
				inputType = "textarea";
				inputValue = $.trim( $requiredField.find( "textarea" ).val() );
				if ( inputValue.length ) {
					isValidField = true;
				}								
			}
			
			// validate 'email' field
			if ( $requiredField.hasClass( "email" ) ) {
				inputType = "email";
				inputValue = $requiredField.find( "input" ).val();
				isValidField = formValidationObj.regexValues.email.test( inputValue );
			} 
			
			// validate 'phone' field
			if ( $requiredField.hasClass( "phone" ) ) {
				inputType = "phone";
				inputValue = $requiredField.find( "input" ).val();
				isValidField = formValidationObj.regexValues.phone.test( inputValue );
			} 
			
			// validate 'date' field
			if ( $requiredField.hasClass( "date" ) ) {
				inputType = "date";
				inputValue = $requiredField.find( "input" ).val();
				isValidField = moment( inputValue, formValidationObj.dateFormat, true ).isValid();
			} 
			
			// validate 'file'
			if ( $requiredField.hasClass( "file" ) ) {
				inputType = "file";
				inputValue = $requiredField.find( "input:file" ).val();
				if ( inputValue.length ) {
					inputValue = inputValue.split('.');
					inputValue = inputValue[ inputValue.length - 1 ];
					if ( $.inArray( inputValue, formValidationObj.fileTypes ) > -1 ) {
						isValidField = true;
					} 
				}
										
			}
			
			if ( $requiredField.hasClass( "checkbox" ) ) { 
				inputType = "checkbox";
				if ( $requiredField.find( "input[type='checkbox']:checked" ).val() ) {
					isValidField = true;
					$requiredField.find( "input[type='checkbox']:checked" ).each(function(){
						inputValue += $(this).val() + ', ';
					});
					inputValue = inputValue.replace(/,\s*$/, "");
				}
			}
					
			if ( $requiredField.hasClass( "radio" ) ) { 
				inputType = "radio";
				if ( $requiredField.find( "input[type='radio']:checked" ).val() ) {
					isValidField = true;
					inputValue = $requiredField.find( "input[type='radio']:checked" ).val();
				} 
			}
			
			return { 'label': inputLabel, 'value': inputValue, 'type' : inputType, 'isValid' : isValidField };
			
		} // if $requiredField defined
		
	};
	
	var _hideBusyScreen = function() {
		if ( busyScreenEl.length && el.hasClass('active') ) {
			busyScreenEl.removeClass('active');
		}
	};
	
	var _showBusyScreen = function() {
		if ( busyScreenEl.length ) {
			busyScreenEl.addClass('active');
		}
	};
	
	var _submitThisForm = function( $formContainerElement, $debugThis ) {
		
		if ( !_.isEmpty( $formContainerElement ) ) {
			
			var isFormValid = false;
			var formEl = $formContainerElement.find("form");
			var thisForm;
			var confirmationDetails = {};
	
			formEl.on( "click", "input[type=submit]", function( event ) {
				
				var requiredFieldCount = 0;
				var formResults = [];
				var thisButton = $(this);
				
				thisForm = $(this).closest("form");
				
				thisForm.find(".required-field").each(function(){
								
					var thisRequiredField = $(this);
								
					if ( _validateField( thisRequiredField ).isValid ) {
						thisRequiredField.removeClass("error");
						thisRequiredField.find(".error-message").fadeOut( 200, "easeOutBack" );
					} else {
						thisRequiredField.addClass("error");
						thisRequiredField.find(".error-message").fadeIn( 1000, "easeOutBack" );
					}
								
					formResults[requiredFieldCount] = _validateField( thisRequiredField );
														
					requiredFieldCount++;
								
				}); // each '.required-field
				
				$.each( formResults, function( index, val ) {
								
					if ( val.isValid === true ) {
						// do nothing
						isFormValid = true;
						return true;
					} else {
						// break $.each and return false
						isFormValid = false;
						return false;
					}
							   				    
				});
				
				if ( true === $debugThis ) {
					
					console.log( "[ _submitThisForm() ] Debugger Enabled - Form Will Not Submit!" );
					console.log( "[ _submitThisForm() ] Form Results : " + JSON.stringify( formResults, null, 2 ) );
					console.log( "[ _submitThisForm() ] Form Validity : " + isFormValid );
					event.preventDefault();
					
				} else {
			
					if ( isFormValid ) {
						
						_showBusyScreen();
						thisForm.find(".error-message.general").fadeOut( 200, "easeOutBack" );
						thisForm.submit();
						
					} else {
						
						thisForm.find(".error-message.general").fadeIn( 1000, "easeOutBack" );
						event.preventDefault();
					}
					
				}
				
			}); // on 'click' for 'input[type=submit]'
			
		}
				
	};

	//////////////////////////////////////////////////////////
	////  Public Methods
	//////////////////////////////////////////////////////////
			
	var init = function( $params ) {
      
      	Debug.log( debugThis, info, 'start' );
      	
      	$(document).bind('gform_post_render', function(){
			_hideBusyScreen();
		});
      
      	_setupForm( $('.form--gravity-form') );
      	_submitThisForm( $('.form--gravity-form'), false ); // if set to 'true' debuggin mode is enabled
            
		Debug.log( debugThis, info, 'stop' );	
      
	};
	
	//////////////////////////////////////////////////////////
	////  Returned Methods
	//////////////////////////////////////////////////////////
	
	return {
      	info : info,
		init : init
	};

})();