<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Filename: page.php
*
*/

get_header();

?>

<div id="page" class="page page--default" role="main">
		
	<?php if ( have_posts() ) : ?>	
		<?php while ( have_posts() ) : the_post(); ?>
		
			<div class="block block--default-content">
				<div class="wrapper"><div class="row"><div class="col-xs-12 col-md-10 col-md-offset-1">
					
					<?php if ( get_the_content() ) : ?>
				
						<div class="rte">
							<?php the_content(); ?>
						</div>
						
					<?php endif; ?>
					
				</div></div></div>
				<!-- /.wrapper .row .col -->
				
			</div>
			<!-- /.block--default-content -->
		
		<?php endwhile; ?>
	<?php else : ?>
		<!-- No Post Content -->
	<?php endif; wp_reset_postdata(); ?>		

</div>
<!-- /#page -->	
	
<?php get_footer(); ?>