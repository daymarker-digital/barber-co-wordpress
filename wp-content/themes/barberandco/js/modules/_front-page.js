//////////////////////////////////////////////////////////
////  Front Page
//////////////////////////////////////////////////////////

var FrontPage = (function () {
	
	var debugThis = false;
	var info = { name : 'FrontPage', version : 1.1 };
	var offsetSections = $(".section--colour-theme");
	var debuggingEl = $("#front-page .debugging");
	var arrOfSections = [];
	var scrollDirection = 'down';
	var lastScrollTop = 0;
	
	//////////////////////////////////////////////////////////
	////  Private Methods
	//////////////////////////////////////////////////////////
	
	var _scrollWatcher = function () {
		
		var windowVerticalOffset = window.pageYOffset;
									
		$(".section--colour-theme").each( function( index ) {
				
			var thisElement = $(this);
								
			var thisOffset = {
				'theme' : thisElement[0].dataset.theme,
				'index' : index,
				'top' : thisElement[0].offsetTop,
				'bottom' : thisElement[0].offsetTop + thisElement[0].offsetHeight,
				'height' : thisElement[0].offsetHeight
			};
				
			switch ( scrollDirection ) {
				case 'down':
					// scrolling down page			
					if ( ( windowVerticalOffset >= thisOffset.top ) && ( windowVerticalOffset < thisOffset.bottom ) ) {
						_setActiveColourTheme( thisOffset.theme );
						return;
					}
					return;
					break;
				case 'up':
					// scrolling up page			
					if ( ( windowVerticalOffset >= ( thisOffset.top - ( thisOffset.height * 0.05 ) ) ) && ( windowVerticalOffset < thisOffset.bottom ) ) {
						_setActiveColourTheme( thisOffset.theme );
						return;
					} 
					return;
				break;
			}
								
		});
			
	};
	
	var _setActiveColourTheme = function ( $colourTheme ) {
		
		$("header.offset-theme").removeClass("light dark");
	
		switch ( $colourTheme ) {
			case 'light':
				$("header.offset-theme").addClass('light');
				break;
			case 'dark':
				$("header.offset-theme").addClass('dark');
				break;
		}
		
	};

	//////////////////////////////////////////////////////////
	////  Public Methods
	//////////////////////////////////////////////////////////
	
	var headerColourTheme = function () {
				
		if ( $("#front-page").length ) {
			
			var resizeTimer; 
			var lastScrollPosition = 0;	
			
			_scrollWatcher();
			
			$(window).on( "resize", function( event ){
				
				clearTimeout( resizeTimer );
				resizeTimer = setTimeout( _scrollWatcher, 50 );
				
			});
			  
			$(window).on( "scroll", function( event ){
								
				var newScrollPosition = window.scrollY;
				if ( newScrollPosition < lastScrollPosition ) {
					scrollDirection = 'up';
				} else {
					scrollDirection = 'down';
				}
				
				lastScrollPosition = newScrollPosition;
			
				clearTimeout( resizeTimer );
				resizeTimer = setTimeout( _scrollWatcher, 50 );
				
			});
			
		} else {
			if( debugThis ) {
				console.log( '[ offsetTheme() ] Error: #front-page does not exist' );
			}
		}
		
	};
			
	var init = function( $params ) {
      
		Debug.log( debugThis, info, 'start' );
				
		headerColourTheme();
				
		Debug.log( debugThis, info, 'stop' );	
      
	};
	
	//////////////////////////////////////////////////////////
	////  Returned
	//////////////////////////////////////////////////////////
	
	return {
		info : info,
		init : init
	};
	
})();