<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Template Name: Page [ Contact Us ]
*	Filename: page-contact-us.php
*
*/

get_header();
 
// ACF Vars

?>

<div id="contact-us" class="page page--contact-us" role="main">
	
	<?php if ( get_field( "cover-image" ) ) : ?>
		<!-- Cover Image -->
		<?php include( locate_template( "./includes/common--cover.php" ) ); ?>
		<!-- End of Cover Image -->
	<?php endif; ?>
		
	<?php if ( get_field( "message" ) || get_the_title() ) : ?>
		<div class="content">
			<div class="wrapper"><div class="row"><div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
				
				<?php if ( get_the_title() ) : ?>
					<h1 class="headline headline--section-title"><?php the_title(); ?></h1>
				<?php endif; ?>
								
				<?php if ( get_field( "message" ) ) : ?>
					<div class="message message--rte">
						<?php the_field( "message" ); ?>
					</div>
					<!-- /.message -->
				<?php endif; ?> 
				
			</div></div></div>
			<!-- /.wrapper .row .col -->
		</div>
		<!-- /.message -->
	<?php endif; ?>
	
	<?php if ( get_field( "form" ) ) : ?>
	
		<?php 
			$form = get_field( "form" );
			$formTitle = $form['title'];
			$formTitleSlug = clean_string( $formTitle );
		?>
	
		<div class="form form--gravity-form form--gravity-forms-<?php echo $formTitleSlug;?>">
			
			<div class="wrapper"><div class="row"><div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
				
				<?php
					gravity_form_enqueue_scripts( $form['id'], true );
					gravity_form( $form['id'], true, true, false, '', true, 1 ); 
				?>
				
			</div></div></div>
			<!-- /.wrapper .row .col -->
			
		</div>
		<!-- /.form--gravity-form -->
		
	<?php else: ?>	
		<!-- NO FORM!!! -->
	<?php endif; ?>
				
</div>
<!-- /#contact-us -->

<?php get_footer(); ?>