<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Filename: layout--instagram.php
*
*/

// Theme Vars
$home = Daymarker( 'home_url' );
$shopify_url = Daymarker( 'shopify_url' );
$template_dir = Daymarker( 'template_dir' );
$is_production = Daymarker( 'production' );
$is_maintenance = Daymarker( 'maintenance' );
$theme_classes = Daymarker( 'theme_classes' );

// Default Vars
$message_colour_theme = $message_background_colour = $instagram_account = $instagram_url = $image_display = $image_background_colour = false;
$instagram_icon = array(
	'beige' => 'BARBER--web-content--icon--instagram--beige.svg',
	'dark-grey' => 'BARBER--web-content--icon--instagram--dark-grey.svg'
);
$instagram_icon_src = $instagram_icon['beige'];
if ( get_sub_field( 'colour_theme' ) ) {
	$message_colour_theme = get_sub_field( 'colour_theme' );	
	if ( 'dark' !== $message_colour_theme ) {
		$instagram_icon_src = $instagram_icon['dark-grey'];
	}
}
if ( get_sub_field( 'message_background_colour' ) ) {
	$message_background_colour = get_sub_field( 'message_background_colour' );	
}
if ( get_sub_field( 'instagram_account' ) ) {
	$instagram_account = get_sub_field( 'instagram_account' );	
}
if ( get_sub_field( 'instagram_url' ) ) {
	$instagram_url = get_sub_field( 'instagram_url' );	
}
if ( get_sub_field( 'image_display' ) ) {
	$image_display = get_sub_field( 'image_display' );	
	$instafeed_options = array(
		"display" => $image_display
	); 
	$instafeed_options_json = json_encode( $instafeed_options );
	wp_localize_script( 'js-daymarker', 'wp_localized_script__instafeed_options', $instafeed_options_json );
}

if ( get_sub_field( 'image_background_colour' ) ) {
	$image_background_colour = get_sub_field( 'image_background_colour' );	
}


// Default Vars
$insta_images = array();
$result_data = $result_json['data'];
	
// Iterate through results
foreach ( $result_data as $result ) {
				
		$insta_image = array();
		$insta_image['img_url'] = false;
		$insta_image['caption'] = false;
		$insta_image['post_link'] = false;
		
		if ( isset( $result['images']['standard_resolution']['url'] ) && ! empty( $result['images']['standard_resolution']['url'] ) ) {
			$insta_image['img_url'] = $result['images']['standard_resolution']['url'];
		}
		if ( isset( $result['caption']['text'] ) && ! empty( $result['caption']['text'] ) ) {
			$insta_image['caption'] = $result['caption']['text'];
		}
		if ( isset( $result['link'] ) && ! empty( $result['link'] ) ) {
			$insta_image['post_link'] = $result['link'];
		}
			
		// Add results to array	
		array_push( $insta_images, $insta_image );
		
}
		
if ( !empty( $insta_images ) ) { 
		
	echo '<div class="carousel-container carousel-container--front-page carousel-container--split carousel-container--instagram" id="front-page-instagram-carousel--0' . $row_count . '">';

	echo '<div class="owl-carousel">';
		
	echo '<div class="block block--01 block--instagram">';
	if ( $message_background_colour ) {
		echo '<div class="background-colour" style="background:' . $message_background_colour . ';"></div>';
		echo '<!-- /.background-colour -->';
	}
	echo '<div class="content content--instagram content--' . $message_colour_theme . '">';
	echo '<span class="instagram__account"><a href="' . $instagram_url . '" target="_blank">@' . $instagram_account . '</a></span>';
	echo '<span class="instagram__message">' . $insta_images[0]['caption'] . '</span>';
	echo '<span class="instagram__icon"><a href="' . $instagram_url . '" target="_blank"><img src="' . $template_dir . '/img/social/' . $instagram_icon_src . '" alt="Instagram Icon"/></a></span>';
	echo '</div>';
	echo '<!-- /.content--instagram -->';
	echo '</div>';
	echo '<!-- /.block--01 -->';
			
	echo '<div class="block block--02 block--instagram">';
	switch ( $image_display ) {
		case 'inline-image':
			if ( $image_background_colour ) {
				echo '<div class="background-colour" style="background:' . $image_background_colour . ';"></div>';
			}
			echo '<div class="inline-image lazyload-container lazyload-container--inline-image">';
			echo '<a href="' . $insta_images[0]['post_link'] . '" class="instagram__post-link instagram__post-link--inline" target="_blank">';
			echo '<img class="lazyload lazyload--inline-image" src="" data-src="' . $insta_images[0]['img_url'] . '" alt="Barber & Co Instagram Post"/>';
			echo '</a>';
			echo '</div>';
			break;
		case 'background-image':
			echo '<div class="background-image lazyload-container lazyload-container--background-image">';
			echo '<a href="' . $insta_images[0]['post_link'] . '" target="_blank" class="instagram__post-link instagram__post-link--background lazyload lazyload--background-image" data-bg="' . $insta_images[0]['img_url'] . '"></a>';
			echo '</div>';
			break;
	}
	echo '</div>';	
	echo '<!-- /.block--02 -->';
		
			
	echo '</div>';
	echo '<!-- /.owl-carousel -->';
			
	echo '</div>';
	echo '<!-- /.carousel-container--front-page-split -->';
		
}	

?>