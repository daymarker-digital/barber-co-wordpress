<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html <?php language_attributes(); ?> class="no-js"> <!--<![endif]-->

	<?php
	
		//////////////////////////////////////////////////////////
		////  Theme Vars
		//////////////////////////////////////////////////////////
	
		$home = Daymarker( 'home_url' );
		$template_dir = Daymarker( 'template_dir' );
		$shopify_url = Daymarker( 'shopify_url' );
		
		$is_production = Daymarker( 'production' );
		$is_maintenance = Daymarker( 'maintenance' );
		$is_coming_soon = Daymarker( 'coming-soon' );
		
		$theme_classes = Daymarker( 'theme_classes' );
		$post_id = Daymarker( 'post_ID' );
		$post_type = Daymarker( 'post_type' );
		
	?>
			
	<meta charset="<?php bloginfo('charset'); ?>">
	
	<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
	
	<link href="//www.google-analytics.com" rel="dns-prefetch">
   
	<link href="<?php echo $template_dir . '/apple-touch-icon.png?v=' . filemtime( get_template_directory() . '/apple-touch-icon.png' ); ?>" rel="apple-touch-icon">
  <link href="<?php echo $template_dir . '/favicon.ico?v=' . filemtime( get_template_directory() . '/favicon.ico' ); ?>" rel="shortcut icon">
    
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="Daymarker Digital">
	<meta http-equiv="Expires" content="7" />
	
	<?php 
						
		// Check to see if site is production or staging
		if ( $is_production ) {
			
			if ( is_attachment() ) {
				echo '<meta name="robots" content="noindex, nofollow">';
			}
			
			// Check for YOAST Plugin
			if ( defined('WPSEO_VERSION') ) {
				echo '<!-- Yoast Plugin IS Active! -->';
			} else {
				echo '<!-- Yoast Plugin IS NOT Active! -->';
				echo '<meta name="description" content="' . get_bloginfo('description') . '">';
			}
			
			// Google Analytics
			include( locate_template( './includes/common--google-analytics.php' ) );
						
		} else {
			echo '<meta name="robots" content="noindex, nofollow">';
		}
				
		// Header Scripts
		echo '<!-- Header Scripts -->';
		include( locate_template( './includes/common--scripts.php' ) );
		echo '<!-- End of Header Scripts -->';
					
		// Inline Main CSS	
		echo '<!-- Main CSS -->';
		echo '<style>';
		include( locate_template( './css/main.css' ) );
		echo '</style>';
		echo '<!-- End of Main CSS -->';
		
		wp_head();
				
	?>
			
	</head>
	
	<body class="<?php echo $theme_classes; ?>"> 
		
		<?php
			
			if ( is_user_logged_in() || is_admin() ) {
				echo "<!-- PHP Version : " . Daymarker('php_version') . " -->";
				echo "<!-- WP Version : " . Daymarker( 'wp_version' ) . " -->";
				echo "<!-- Current Template : " . Daymarker( 'current_template' ) . " -->";
				echo "<!-- Post Type : " . Daymarker( 'current_template' ) . " -->";
				echo "<!-- Post ID : " . Daymarker( 'post_ID' ) . " -->";
			} else {
				if ( $is_maintenance ) {
					include( locate_template( './includes/common--maintenance-mode.php' ) );
				}
			}
			
			//////////////////////////////////////////////////////////
			////  Loading Screen
			//////////////////////////////////////////////////////////

			include( locate_template( './includes/common--loading-screen.php' ) );
			
			//////////////////////////////////////////////////////////
			////  Push Content | Left
			//////////////////////////////////////////////////////////
			
			include( locate_template( './includes/common--push-content-left.php' ) ); 
			
			//////////////////////////////////////////////////////////
			////  Push Content | Right
			//////////////////////////////////////////////////////////
			
			include( locate_template( './includes/common--push-content-right.php' ) ); 
			
			//////////////////////////////////////////////////////////
			////  Header
			//////////////////////////////////////////////////////////
			
			include( locate_template( './includes/common--header.php' ) ); 
			
		?>
				
		<div id="site-container" class="<?php echo $theme_classes; ?>">	