<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Filename: common--maintenance-mode.php
*
*/

// Theme Vars
$home = Daymarker( 'home_url' );
$shopify_url = Daymarker( 'shopify_url' );
$template_dir = Daymarker( 'template_dir' );
$is_production = Daymarker( 'production' );
$is_maintenance = Daymarker( 'maintenance' );
$theme_classes = Daymarker( 'theme_classes' );

?>
	
<style>

	#maintenance-mode {
      	width: 100%;
      	height: 100%;
     	background: #282524;
     	color: #f7f5f0;
      	position: fixed;
      	top: 0px;
      	z-index: 99999999999;     
  	}   
  	
  	#maintenance-mode .brand--monogram {
	  	width: 64px;
	  	height: auto;
	  	margin: 0px 0px 30px 0px;
  	}
  
  	#maintenance-mode .headline--section-title {
      	margin: 0px;
      	line-height: 90%;
      	color: inherit;
      	font-weight: 500;
      	font-size: 16px;
      	margin: 0px 0px 10px 0px;
    }
    
    #maintenance-mode .message {
	    color: inherit;
	    font-size: 14px;
	}
	
	 #maintenance-mode .message a {
		display: inline-block;
		-webkit-transition: all 280ms ease-in-out 35ms;
		-moz-transition: all 280ms ease-in-out 35ms;
		-ms-transition: all 280ms ease-in-out 35ms;
		-o-transition: all 280ms ease-in-out 35ms;
		transition: all 280ms ease-in-out 35ms;
		-webkit-backface-visibility: hidden;
	}
	
	#maintenance-mode .message a:hover {
		color: #ddd;
	}
  
  	#maintenance-mode img {
      	display: block;
      	width: 100%;
    }
  
  	.maintenance-mode__main {
        position: relative;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -moz-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        -o-transform: translateY(-50%);
        transform: translateY(-50%);
  	}  	
  
</style>


<div id="maintenance-mode">
	
	<div class="maintenance-mode__main">
		
		<div class="wrapper"><div class="row"><div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
		
		<div class="brand brand--monogram">
			<img src="<?php echo get_template_directory_uri(); ?>/img/brand/BARBER-web-content-logo-symbol-beige.svg" alt="Barber & Co. Monogram" />
		</div>
		<h2 class="headline headline--section-title">Coming Soon</h2>
		<p class="message">Please visit us at <a href="http://www.unionofbarbers.com/">unionofbarbers.com</a></p>
		
		</div></div></div>
		<!-- /.wrapper .row .col -->
		
	</div>
	<!-- /.maintenance-mode__main -->

</div>
<!-- /#maintenance-mode -->