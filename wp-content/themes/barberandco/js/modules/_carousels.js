//////////////////////////////////////////////////////////
////  Carousels
//////////////////////////////////////////////////////////

var Carousels = (function () {

	var debugThis = false;
	var info = { name : 'Carousels', version : 3.6969 };
	var containerClass = '.carousel-container';
	var navigationClasses = {
		next : '.carousel-controls--next, .carousel-nav--next, .carousel-nav.next, .carousel-nav--next',
		prev : '.carousel-controls--prev, .carousel-nav--prev, .carousel-nav.prev, .carousel-nav--prev'
	};

	//////////////////////////////////////////////////////////
	////  Private Methods
	//////////////////////////////////////////////////////////

	var owlCarousels = function () {

		if ( debugThis ) {
  		console.log('started');
    }

		var target = $( containerClass );
		var carousels = [];
		var settings = {
			default : {
				autoplay: 0,
				margin: 0,
		        loop: true,
		        nav: 0,
		        dots: true,
		        autoplayHoverPause: 1,
		        smartSpeed: 450,
		        autoplayTimeout: 3250,
		        autoHeight: 0,
				items: 1,
				onInitialized: carouselCallback
			},
			split : {
				autoplay: 0,
				margin: 0,
				loop: true,
			    nav: 0,
			    dots: 1,
			    autoplayHoverPause: 1,
			    smartSpeed: 500,
			    autoplayTimeout: 3250,
			    autoHeight: 0,
				items: 1,
				onInitialized: carouselCallback
			}
		};

		// iterate over each carousel element
		target.each(function(){

			if ( $(this).find('.owl-carousel').length ) {

				var thisEl = $(this);
				var thisID = thisEl.attr( 'id' );
				var thisCarousel = $( '#' + thisID + ' .owl-carousel' );
				var isSplit = false;
				var thisSettings = settings.default;

				if ( thisEl.hasClass( 'carousel-container--split' ) ) {
					thisSettings = settings.split;
					isSplit = true;
				}

				// add carousel to array
				carousels.push({
					id : thisID,
					carousel : thisCarousel,
					settings : thisSettings,
					split : isSplit
				});

			}

      if ( debugThis ) {
        console.log( carousels );
      }

		});

		//////////////////////////////////////////////////////////
		////  Carousel Initialization & Setup
		//////////////////////////////////////////////////////////

		$.each( carousels, function( index, item ) {

			var carousel = item.carousel;
			var carouselID = item.id;
			var split = item.split;
			var settings = item.settings;

			if ( split && window.innerWidth > 992 ) {

				carousel.owlCarousel('destroy');
				$("#" + carouselID).addClass('destroyed');

			} else {

				// initialize event callback on owl carousel initialize
				carousel.on( 'initialize.owl.carousel initialized.owl.carousel', function( event ) {
					$("#" + carouselID).removeClass('destroyed');
				});

				// initialize carousel
				carousel.owlCarousel( settings ).trigger('refresh.owl.carousel');

				// refresh all he carousels after loaded to fix single item gallery width issue
				$('.owl-carousel').trigger('refresh.owl.carousel');

			}

		});

		//////////////////////////////////////////////////////////
		////  Carousel Navigation
		//////////////////////////////////////////////////////////

		$('.carousel__dot').on('click', function(event) {
			var carouselID = $(this).closest( containerClass ).attr('id');
			$("#" + carouselID + " .owl-carousel").trigger('to.owl.carousel', [ $(this).index(), 360 ] );
		});

		$( navigationClasses.prev ).on("click", function(event) {
      var carouselID = $(this).closest( containerClass ).attr('id');
      $("#" + carouselID + " .owl-carousel").trigger('prev.owl.carousel', [360]);
    });

    $( navigationClasses.next ).on("click", function(event) {
      var carouselID = $(this).closest( containerClass ).attr('id');
      $("#" + carouselID + " .owl-carousel").trigger('next.owl.carousel', [360]);
    });

    //////////////////////////////////////////////////////////
		////  Carousel Callback
		//////////////////////////////////////////////////////////

    function refreshMe ( event ) {

      if ( debugThis ) {
        console.log( event );
      }

      var thisClasses = event.target.offsetParent.className;
      var thisId = event.target.offsetParent.id;

      $("#" + thisId + " .owl-carousel").find('.carousel__item').removeClass('refreshed');

      setTimeout( function(){

		    $("#" + thisId).find('.carousel__loading-screen').removeClass('active');
				$("#" + thisId + " .owl-carousel").trigger('refresh.owl.carousel');
				$("#" + thisId + " .owl-carousel").find('.carousel__item').addClass('refreshed');
				$("#" + thisId).find('.carousel-nav').addClass('refreshed');

			}, 500 );

    }

    var carouselCallback = function() {
		  // nothing to call back
		}

    //////////////////////////////////////////////////////////
		////  REFACTOR THIS SHIT
		//////////////////////////////////////////////////////////

      var conditionallyResetCarousel = function () {

			if ( debugThis ) {
				console.log( '[ _conditionallyResetCarousel() ] Initialized' );
			}

			$.each( carousels, function( index, value ) {

				if ( debugThis ) {
					console.log( value );
				}

				// get carousel details
				var thisCarousel = value.carousel;
				var	settings = value.settings;
				var thisCarouselID = value.id;
				var isSplit = value.split;

				if ( isSplit ) {

					if ( window.innerWidth > 992 ) {
						thisCarousel.owlCarousel( 'destroy' );
						$( '#' + thisCarouselID ).addClass( 'destroyed' );
					} else {
						thisCarousel.owlCarousel( settings );
						$( '#' + thisCarouselID ).removeClass('destroyed');
					}

				}

			}); // each array item

			if ( debugThis ) {
				console.log( '[ _conditionallyResetCarousel() ] Complete' );
			}

		};

        $(window).on('resize', function(){
			conditionallyResetCarousel();
		});

	};

	//////////////////////////////////////////////////////////
	////  Public Method | Initialize
	//////////////////////////////////////////////////////////

	var init = function() {

		Debug.log( debugThis, info, 'start' );

		owlCarousels();

		Debug.log( debugThis, info, 'stop' );

	};

	//////////////////////////////////////////////////////////
	////  Returned Methods
	//////////////////////////////////////////////////////////

	return {
		info : info,
		init : init
	};

})();
