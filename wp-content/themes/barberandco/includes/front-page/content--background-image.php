<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Filename: content--background-image.php
*
*/

// Default Vars
$image = $image_alt = $image_src = false;

// message vars
$message_obj = array();
$message_obj['colour_theme'] = get_sub_field( 'colour_theme' );
$message_obj['message'] = get_sub_field( 'message' );
$message_obj['cta'] = get_sub_field( 'cta' );
$message_obj['link'] = get_sub_field( 'link' );
$message_obj['pos_hor'] = get_sub_field( 'pos_hor' );
$message_obj['pos_vert'] = get_sub_field( 'pos_vert' );

// Get Values
if ( get_sub_field( 'image' ) ) {

	$image = get_sub_field( 'image' );
	
	$image_src = $image['url'];
	$image_alt = $image['alt'];
	
	echo '<div class="background-image lazyload-container lazyload-container--background-image js--vpa-match-viewport-height">';
		
	if ( $image_src ) {
		echo '<div class="lazyload lazyload--background-image" data-bg="' . $image_src . '"></div>';
	}
		
	include( locate_template( './includes/front-page/content--message.php' ) );		
		
	echo '</div>';
	echo '<!-- /.background-image -->';

} else {
	echo '<!-- No Image -->';
}

?>