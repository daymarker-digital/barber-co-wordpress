<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Template Name: Page [ Thank You ]
*	Filename: page-thank-you.php
*
*/

get_header();
 
?>

<div id="thank-you" class="page page--thank-you" role="main">
	
	<div class="js--vpa-match-viewport-height">
		
		<div class="error-message error-message--404">
		
			<div class="wrapper"><div class="row"><div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
						
				<h1 class="headline headline--error">Thank You!</h1>
				<p>Please expect to hear back from our team shortly.<br> Let us take you <a href="<?php echo get_home_url(); ?>">home</a>.</p>
						
			</div></div></div>
			<!-- /.wrapper .row .col -->
				
		</div>
		
	</div>
			
		
</div>
<!-- #page.thank-you -->	
	
<?php get_footer(); ?>