//////////////////////////////////////////////////////////
////  Main Menu
//////////////////////////////////////////////////////////

var MainMenu = (function () {

	var debugThis = false;
	var info = { name : 'MainMenu', version : 1.1 };
	
	//////////////////////////////////////////////////////////
	////  Private Methods
	//////////////////////////////////////////////////////////
		
	var _hover = function () {
            	
		$(".navigation-item--main").hover(function () {
        	$(this).find(".navigation--sub").stop(true,true).fadeIn(350);
        }, function () {
            $(this).find(".navigation--sub").stop(true,true).delay(175).fadeOut(175);
        });
    
        $("header").on("mouseenter", function(){
            $(this).addClass("hover");
        }).on("mouseleave", function(){
            $(this).removeClass("hover");
        });
	        
    };
    
	//////////////////////////////////////////////////////////
	////  Public Methods
	//////////////////////////////////////////////////////////
			
	var init = function( $params ) {
      
		Debug.log( debugThis, info, 'start' );
		_hover();
		Debug.log( debugThis, info, 'stop' );	
      
	};
	
	//////////////////////////////////////////////////////////
	////  Returned
	//////////////////////////////////////////////////////////
	
	return {
		info : info,
		init : init
	};

})();