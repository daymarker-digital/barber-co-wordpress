<?php
	
/*
*  Theme : Barber + Co
*	Template Name: Category Default
*
*/

get_header();
 
// ACF Vars

?>


<div id="main-content" class="post category">
	
		<?php get_template_part( 'includes/content-journal-cover' ); ?>	
				
		<?php get_template_part( 'includes/content-category-posts' ); ?>
		
		<div class="offset-background"></div>
		<!-- /.offset-background -->
		
</div>
<!-- /#main-content.journal -->

<?php get_footer(); ?>