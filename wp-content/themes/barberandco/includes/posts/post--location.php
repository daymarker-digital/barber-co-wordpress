<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Filename: post--location.php
*
*/

// Theme Vars
$home = Daymarker( 'home_url' );	
$template_dir = Daymarker( 'template_dir' );
$is_production = Daymarker( 'production' );
$is_maintenance = Daymarker( 'maintenance' );
$theme_classes = Daymarker( 'theme_classes' );

?>

<div id="location">
	
	<div class="location__main">
		
		<div class="column-container">
          
      <!-- Left Column -->
			<div class="column column--left js--vpa-match-viewport-height">
				
				<div class="location-images location-images--main">
				
					<?php if ( have_rows( "gallery" ) ) : ?>
					
						<?php
							$is_gallery = false;
							$row_count = count( get_field( "gallery" ) );
							$carousel_icon_nav = $template_dir . "/img/ui/BARBER-web-content-ui-icon-carousel-nav-next-beige.svg";
							if ( $row_count > 1 ) :
								$is_gallery = true;
							endif;
						?>
						
						<div class="carousel-container carousel-container--location carousel-container--location-gallery js--vpa-match-viewport-height" id="carousel-container--location-gallery">
	
							<?php if ( $is_gallery ) : ?><div class="carousel owl-carousel carousel--feature"><?php endif; ?>
					
							<?php while ( have_rows( "gallery" ) ) : the_row(); ?>
							
								<?php 
									// Gallery Sub Fields
									$image = $image_src = false;
									if ( get_sub_field( "image" ) ) {
										$image = get_sub_field( "image" );
										$image_src = $image['url'];
									}
								?>
								
								<li class="carousel-item carousel-item--location-main js--match-viewport-height lazyload-container lazyload-container--background-image js--vpa-match-viewport-height">
									<div class="lazyload lazyload--background-image" <?php if ( $image_src ) : ?>data-bg="<?php echo $image_src; ?>"<?php endif; ?>></div>
								</li>
								<!-- /.carousel-item--location-main -->
							
							<?php endwhile; ?>
							
							<?php if ( $is_gallery ) : ?>
								</div>
								<!-- /.owl-carousel--> 
					
								<div class="carousel-controls carousel-controls--prev prev"><img src="<?php echo $carousel_icon_nav; ?>" alt="Carousel Navigation Icon 'Previous'"/></div>
								<div class="carousel-controls carousel-controls--next next"><img src="<?php echo $carousel_icon_nav; ?>" alt="Carousel Navigation Icon 'Next'"/></div>
	                        <?php endif; ?>
							
						</div>
						<!-- /.carousel-container -->
					
					<?php else : ?>
						<!-- NO GALLERY IMAGE(S) -->
					<?php endif; ?>
					
				</div>
				<!-- /.location-images--main -->
				
			</div>
			<!-- End of Left Column -->
			
			<!-- Right Column -->
			<div class="column column--right js--vpa-match-viewport-height" data-vpa-settings='{ "limit":992, "include_header":false, "include_fooer":false }'>
				
				<div class="location-details">
					
					<?php if ( get_the_title() ) : ?>
						<h1 class="headline headline--location-title"><?php echo the_title(); ?></h1>
					<?php endif; ?>
					<?php if ( get_field( "city" ) ) : ?>
						<span class="location-city"><?php the_field( "city" ); ?></span>
					<?php endif; ?>
					
					
					<?php if ( have_rows( "locationDetails" ) ) : $row_count = 0; ?>
						<ul class="tabs-navigation">
							<?php while ( have_rows( "locationDetails" ) ) : the_row(); ?>
							
								<?php
									// ACF Sub Fields
									$heading = get_sub_field( "heading" );
								?>
							
								<?php if ( $heading ) : ?>
									<li class="tabs-navigation-item <?php if ( $row_count === 0 ) : ?>active<?php endif; ?>"><?php echo $heading; ?></li>
								<?php endif; ?>
								
								<?php $row_count++; ?>
								
							<?php endwhile; ?>
						</ul>
					<?php else : ?>
						<!-- NO LOCATION DETAIL(S)!!! -->
					<?php endif; ?>
					
					<?php if ( have_rows( "locationDetails" ) ) :  $row_count = 0; ?>
						<ul class="tabs-content">
							
							<?php while ( have_rows( "locationDetails" ) ) : the_row(); ?>
							
								<?php
									// ACF Sub Fields
									$message = get_sub_field( "message" );
									$includeCTA = get_sub_field( "includeCTA" );
									$ctaLinkUrl = get_sub_field( "ctaLinkUrl" );
								?>
							
								<li class="tabs-content-item <?php if ( $row_count === 0 ) : ?>active<?php endif; ?>">
									<div class="tab-content-padding">
										<?php if ( $message ) : ?>
											<div class="message message--rte"><?php echo $message; ?></div>
										<?php endif; ?>
										<?php if ( $includeCTA && $ctaLinkUrl ) : ?>
											<a class="button button--inline button--solid-fill button--location-cta" href="<?php echo $ctaLinkUrl; ?>">Book Now</a>
										<?php endif; ?>
									</div>
								</li>
								<!-- /.tabs-content-item-->
								
								<?php $row_count++; ?>
								
							<?php endwhile; ?>
							
						</ul>
					<?php else : ?>
						<!-- NO LOCATION DETAIL(S)!!! -->
					<?php endif; ?>
					
				</div>
				<!-- /.location-details -->
				
			</div>
			<!-- End of Right Column -->
                        
      	</div>
      	<!-- /.js-match--container -->
      	
	</div>
	<!-- /.location__main -->
	
	<div class="location__lifestyle">
		
		<div class="location-images location-images--lifestyle">
				
			<?php if ( have_rows( "lifestyleGallery" ) ) : ?>
					
				<?php
					$is_gallery = false;
					$row_count = count( get_field( "lifestyleGallery" ) );
					$carousel_icon_nav = $template_dir . "/img/ui/BARBER-web-content-ui-icon-carousel-nav-next-beige.svg";
					if ( $row_count > 1 ) :
						$is_gallery = true;
					endif;
				?>
						
				<div class="carousel-container carousel-container--location carousel-container--lifestyle-gallery" id="carousel-container--location-gallery-lifestyle">
	
					<?php if ( $is_gallery ) : ?><div class="carousel owl-carousel carousel--feature"><?php endif; ?>
					
					<?php while ( have_rows( "lifestyleGallery" ) ) : the_row(); ?>
							
						<?php 
							// Gallery Sub Fields
							$image = $image_src = false;
							if ( get_sub_field( "image" ) ) {
								$image = get_sub_field( "image" );
								$image_src = $image['url'];
							}
						?>
								
						<li class="carousel-item carousel-item--location-lifestyle lazyload-container lazyload-container--background-image js--vpa-match-viewport-height">
							<div class="lazyload lazyload--background-image" <?php if ( $image_src ) : ?>data-bg="<?php echo $image_src; ?>"<?php endif; ?>></div>
						</li>
						<!-- /.carousel-item--location-main -->
							
					<?php endwhile; ?>
							
					<?php if ( $is_gallery ) : ?>
						</div>
						<!-- /.owl-carousel--> 
					<?php endif; ?>
							
					<?php if ( $is_gallery ) : ?>
						<div class="carousel-controls carousel-controls--prev prev"><img src="<?php echo $carousel_icon_nav; ?>" alt="Carousel Navigation Icon 'Previous'"/></div>
						<div class="carousel-controls carousel-controls--next next"><img src="<?php echo $carousel_icon_nav; ?>" alt="Carousel Navigation Icon 'Next'"/></div>
	                <?php endif; ?>
							
				</div>
				<!-- /.carousel-container -->
					
			<?php else : ?>
				<!-- NO GALLERY IMAGE(S) -->
			<?php endif; ?>
					
		</div>
		<!-- /.location-images--main -->
		
	</div>
	<!-- /.location__lifestyle -->
	
	<?php if ( have_rows( "barbers" ) ) :?>
	
	  <div class="location__barbers">
		
		  <div class="wrapper"><div class="row"><div class="col-md-10 col-md-offset-1">
			  		
			  <h2 class="headline headline--section-title">Barbers on Location</h2>

				<?php
					// Barbers ACF Vars
					$barber_photo_placeholder = $template_dir . "/img/placeholder/BARBER-placeholder-barber-profile-picture.jpg";
					$instagram_icon = $template_dir . "/img/social/BARBER-web-content-icon-instagram-dark-grey.svg";
				?>
			
				<ul class="barber-list row inner">
					
				<?php while ( have_rows( "barbers" ) ) : the_row(); ?>
				
					<?php
						// Barber Post Objects
						$post_object = get_sub_field( "barber" );
						if ( $post_object ) : 
							$post = $post_object;
							setup_postdata( $post ); 
							
							$barber_name = $barber_instagram_link = false;
							$barber_photo = $barber_photo_placeholder;
							
							if ( get_the_title() ) {
								$barber_name = get_the_title();
							}
							if ( get_field( "photo" ) ) {
								$barber_photo = get_field( "photo" );
								$barber_photo = $barber_photo['url'];
							}
							if ( get_field( "instagram-account" ) ) {
								$barber_instagram_link = get_field( "instagram-account" );
							}
					?>
					
					<li class="barber-list-item col-xs-6 col-md-3">
					
						<div class="barber">
							
							<?php if ( $barber_instagram_link ) : ?><a href="<?php echo $barber_instagram_link; ?>" target="_blank"><?php endif; ?>
							
							<div class="barber__image lazyload-container lazyload-container--background-image">
								<div class="background-image lazyload lazyload--background-image" <?php if ( $barber_photo ) : ?>data-bg="<?php echo $barber_photo; ?>"<?php endif; ?>></div>
							</div>
							<!-- /.barber__image -->
							
							<div class="barber__info">
								
								<?php if ( $barber_name ) : ?>
									<h3 class="headline headline--barber-name"><?php echo $barber_name; ?></h3>
									<!-- /.headline--barber-name -->
								<?php endif; ?>
								
								<?php if ( $barber_instagram_link ) : ?>
									<div class="instagra-icon">
										<img src="<?php echo $instagram_icon; ?>" alt="Instagram Icon"/>
									</div>
								<?php endif; ?>
							
							</div>
							<!-- /.barber__infor -->
							
							<?php if ( $barber_instagram_link ) : ?></a><?php endif; ?>
							
						</div>
						<!-- /.barber -->
					
					</li>
					<!-- /.barber-list-item -->
					
					<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
					
					<?php endif; ?>
				
				<?php endwhile; ?>
				</ul>
					
		</div></div></div>
		<!-- /.wrapper .row .col -->
		
	</div>
	<!-- /.location__barbers -->
	
	<?php endif; ?>
		
</div>
<!-- /#location -->