<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Filename: content--background-colour.php
*
*/

// Default Vars
$bg_colour = false;

// message vars
$message_obj = array();
$message_obj['colour_theme'] = get_sub_field( 'colour_theme' );
$message_obj['message'] = get_sub_field( 'message' );
$message_obj['cta'] = get_sub_field( 'cta' );
$message_obj['link'] = get_sub_field( 'link' );
$message_obj['pos_hor'] = get_sub_field( 'pos_hor' );
$message_obj['pos_vert'] = get_sub_field( 'pos_vert' );

if ( get_sub_field( 'background_colour' ) ) {
	
	$bg_colour = get_sub_field( 'background_colour' );
	
	echo '<div class="background-colour" style="background:' . $bg_colour . ';">';
	
	include( locate_template( './includes/front-page/content--message.php' ) );
	
	echo '</div>';
	echo '<!-- /.background-image -->';
	
}

?>

