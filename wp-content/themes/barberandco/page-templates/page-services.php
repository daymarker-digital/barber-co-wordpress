<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Template Name: Page [ Services ]
*	Filename: page-services.php
*
*/

get_header();

$locations_headline = get_field("locations-headline");
$locations_desc = get_field("locations-description");

?>

<div id="services" class="page page--services" role="main">
	
	<?php if ( get_field( "cover-image" ) ) : ?>
		<!-- Cover Image -->
		<?php include( locate_template( "./includes/common--cover.php" ) ); ?>
		<!-- End of Cover Image -->
	<?php endif; ?>
	
	<section class="services services--hair">		
		
		<div class="services__header">
			<div class="wrapper"><div class="row"><div class="col-sm-10 col-sm-offset-1">
			
				<?php if ( get_field( "hair-headline" ) ) : ?>
					<h2 class="headline headline--section-title"><?php the_field( "hair-headline" ); ?></h2>
				<?php endif; ?>
				
				<?php if ( get_field( "hair-description" ) ) : ?>
					<div class="message message--rte"><?php the_field( "hair-description" ); ?></div>
				<?php endif; ?>
						
			</div></div></div>
			<!-- /.wrapper .row .col -->
	
		</div>
		<!-- /.services__header -->
		
		<?php if ( have_rows( 'hair-services-repeater' ) ) : $repeater_count = count( get_field( 'hair-services-repeater' ) ); ?>
		
			<ul class="services__list services__list--hair services__list--0<?php echo $repeater_count; ?>">
			
				<?php while ( have_rows( 'hair-services-repeater' ) ) : the_row() ; ?>
				
					<?php 
						// ACF Sub Fields
						$image = $image_src = false;
						$service = get_sub_field("service");
						$cost = get_sub_field("cost");
						$attachment_id = get_sub_field("image");
						$image = wp_get_attachment_image_src( $attachment_id, "full" );
						$image_src = $image[0];
					?>
					
					<li class="services__item services__item--hair">
									
						<div class="services__item-background-image lazyload-container lazyload-container--background-image">
							<div class="lazyload lazyload--background-image" <?php if ( $image_src ) : ?>data-bg="<?php echo $image_src; ?>"<?php endif; ?>></div>
						</div>
						<!-- /.services__item-background-image -->
					
						<div class="services__item-overlay"></div>
						<!-- /.services__item-overlay -->
						
						<div class="services__item-cta services__item-cta--hair">
							<h3 class="headline headline--service-title"><?php echo $service . "<br/>" . $cost; ?></h3>
						</div>
						<!-- /.services__item-cta -->
						
					</li>
					<!-- /.services__item -->
					
				<?php endwhile; ?>
			
			</ul>
			<!-- /.services__list -->
				
		<?php endif; ?>
			
	</section>
	<!-- /.services--hair -->
	
	<section class="services services--shave">		
		
		<div class="services__header">
			<div class="wrapper"><div class="row"><div class="col-sm-10 col-sm-offset-1">
			
				<?php if ( get_field( "shave-headline" ) ) : ?>
					<h2 class="headline headline--section-title"><?php the_field( "shave-headline" ); ?></h2>
				<?php endif; ?>
				
				<?php if ( get_field( "shave-description" ) ) : ?>
					<div class="message message--rte"><?php the_field( "shave-description" ); ?></div>
				<?php endif; ?>
						
			</div></div></div>
			<!-- /.wrapper .row .col -->
	
		</div>
		<!-- /.services__header -->
		
		<?php if ( have_rows( 'shave-services-repeater' ) ) : $repeater_count = count( get_field( 'shave-services-repeater' ) ); ?>
		
			<ul class="services__list services__list--shave services__list--0<?php echo $repeater_count; ?>">
			
				<?php while ( have_rows( 'shave-services-repeater' ) ) : the_row() ; ?>
				
					<?php 
						// ACF Sub Fields
						$image = $image_src = false;
						$service = get_sub_field("service");
						$cost = get_sub_field("cost");
						$attachment_id = get_sub_field("image");
						$image = wp_get_attachment_image_src( $attachment_id, "full" );
						$image_src = $image[0];
					?>
					
					<li class="services__item services__item--shave">
									
						<div class="services__item-background-image lazyload-container lazyload-container--background-image">
							<div class="lazyload lazyload--background-image" <?php if ( $image_src ) : ?>data-bg="<?php echo $image_src; ?>"<?php endif; ?>></div>
						</div>
						<!-- /.services__item-background-image -->
					
						<div class="services__item-overlay"></div>
						<!-- /.services__item-overlay -->
						
						<div class="services__item-cta services__item-cta--shave">
							<h3 class="headline headline--service-title"><?php echo $service . "<br/>" . $cost; ?></h3>
						</div>
						<!-- /.services__item-cta -->
						
					</li>
					<!-- /.services__item -->
					
				<?php endwhile; ?>
			
			</ul>
			<!-- /.services__list -->
				
		<?php endif; ?>

	</section>
	<!-- /.services--shave -->
	
</div>
<!-- /#services -->

<?php get_footer(); ?>