<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Filename: post.php
*
*/

get_header();

$post_type = get_post_type( get_the_ID() );
 
?>

<div id="post" class="post post--<?php echo $post_type; ?>" role="main">
	
	<?php
		switch ( $post_type ) {
			case 'location':
			case 'barbershop':
			case 'bar':										
				include( locate_template( "./includes/posts/post--location.php" ) );
				break;
			default:
				break;
		}
	?>
		
</div>
<!-- /#post.<?php echo $post_type; ?> -->

<?php get_footer(); ?>