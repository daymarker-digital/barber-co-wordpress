<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Filename: column-content.php
*
*/

// Inherited Vars
$subField_contentType = $columnSubFields['fields']['contentType'];	
$contentType = get_sub_field( $subField_contentType );
$subField_image = $columnSubFields['fields']['image'];		
$subField_colour = $columnSubFields['fields']['colour'];	
$subField_linkUrl = $columnSubFields['fields']['linkUrl'];	
$subField_cta = $columnSubFields['fields']['cta'];	
$subField_ctaTransparency = $columnSubFields['fields']['ctaTransparency'];
$subField_message = $columnSubFields['fields']['message'];	
$subField_posVert = $columnSubFields['fields']['posVert'];
$subField_posHor = $columnSubFields['fields']['posHor'];
		
?>

<?php switch ( $contentType['value'] ) : case "background-image" : ?>
								
		<?php if ( get_sub_field( $subField_image ) ) : 
			$image = get_sub_field( $subField_image );
			$imageURL = $image['url'];
		?>
			<div class="column-content column-content--background column-content--background-image" style="background-image: url('<?php echo $imageURL; ?>');"></div>
		<?php endif; ?>
										
	<?php break; ?>
								
	<?php case "background-colour" : ?>
										
		<?php if ( get_sub_field( $subField_colour ) ) : 
			$colour = get_sub_field( $subField_colour );
		?>
			<div class="column-content column-content--background column-content--background-colour" style="background: <?php echo $colour; ?>"></div>
		<?php endif; ?>
																	
	<?php break; ?>
									
	<?php case "inline-image" : ?>
									
		<?php if ( get_sub_field( $subField_colour ) ) : 
			$colour = get_sub_field( $subField_colour );
		?>
			<div class="column-content column-content--background column-content--background-colour" style="background: <?php echo $colour; ?>"></div>
		<?php endif; ?>
																				
		<?php if ( get_sub_field( $subField_image ) ) : 
			$image = get_sub_field( $subField_image );
			$imageURL = $image['url'];
			$imageAlt = $image['alt'];
		?>
			<div class="column-content column-content--inline-image">
				<div class="image">
					<img src="<?php echo $imageURL; ?>" alt="<?php echo $imageAlt; ?>" />
				</div>
				<!-- /.column-image -->
			</div>
			<!-- /.column-content -->
		<?php endif; ?>
															
	<?php break; ?>
																	
<?php endswitch; ?>
								
<?php if ( get_sub_field( $subField_linkUrl ) || get_sub_field( $subField_cta ) || get_sub_field( $subField_message ) ) : ?>
								
	<?php 
		// Call to Action Sub Fields
		$linkUrl = get_sub_field( $subField_linkUrl );
		$cta = get_sub_field( $subField_cta );
		$message = get_sub_field( $subField_message );
		$posVert = get_sub_field( $subField_posVert );
		$posHor = get_sub_field( $subField_posHor );
		$ctaTransparency = get_sub_field( $subField_ctaTransparency );
	?>
								
	<div class="column-content column-content--text column-position-vertical--<?php echo $posVert['value']; ?> column-position-horizontal--<?php echo $posHor['value']; ?>">
				
		<div class="content <?php if ( $ctaTransparency ) : ?><?php else : ?>content--background-beige column--drop-shadow<?php endif; ?>">					
			<?php if ( $linkUrl ) : ?><a href="<?php echo $linkUrl; ?>"><?php endif; ?>
				<?php if ( $message ) : ?>
					<p class="message"><?php echo $message; ?></p>
				<?php endif; ?>
				<?php if ( $cta ) : ?>
					<span class="cta"><?php echo $cta; ?></span>
				<?php endif; ?>
			<?php if ( $linkUrl ) : ?></a><?php endif; ?>
		</div>
		<!-- /.content -->
										
	</div>
	<!-- /.column-content--text -->
								
<?php endif; ?>