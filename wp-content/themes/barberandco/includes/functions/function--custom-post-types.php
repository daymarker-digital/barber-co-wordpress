<?php

//////////////////////////////////////////////////////////
////  Custom Post 'Barber'
//////////////////////////////////////////////////////////

function custom_post_type__barber() {

	$labels = array(
		'name'                => _x( 'Barbers', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Barber', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Barbers', 'text_domain' ),
		'name_admin_bar'      => __( 'Barber', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Barber:', 'text_domain' ),
		'all_items'           => __( 'All Barbers', 'text_domain' ),
		'add_new_item'        => __( 'Add New Barber', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'new_item'            => __( 'New Barber', 'text_domain' ),
		'edit_item'           => __( 'Edit Barber', 'text_domain' ),
		'update_item'         => __( 'Update Barber', 'text_domain' ),
		'view_item'           => __( 'View Barber', 'text_domain' ),
		'search_items'        => __( 'Search Barber', 'text_domain' ),
		'not_found'           => __( 'Barber Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Barber Not found in Trash', 'text_domain' ),
	);

	$args = array(
		'label'               => __( 'Barber', 'text_domain' ),
		'description'         => __( 'Posts for Barbers', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'show_in_rest' 		  => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'rewrite'             => array( 'slug' => 'barber' )
	);

	register_post_type( 'barber', $args );

} // custom_post_type__barber()

add_action( 'init', 'custom_post_type__barber' );

//////////////////////////////////////////////////////////
////  Custom Post 'Location'
//////////////////////////////////////////////////////////

function custom_post_type__location() {

	$labels = array(
		'name'                => _x( 'Locations', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Location', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Locations', 'text_domain' ),
		'name_admin_bar'      => __( 'Location', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Location:', 'text_domain' ),
		'all_items'           => __( 'All Locations', 'text_domain' ),
		'add_new_item'        => __( 'Add New Location', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'new_item'            => __( 'New Location', 'text_domain' ),
		'edit_item'           => __( 'Edit Location', 'text_domain' ),
		'update_item'         => __( 'Update Location', 'text_domain' ),
		'view_item'           => __( 'View Location', 'text_domain' ),
		'search_items'        => __( 'Search Location', 'text_domain' ),
		'not_found'           => __( 'Location Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Location Not found in Trash', 'text_domain' ),
	);

	$args = array(
		'label'               => __( 'Location', 'text_domain' ),
		'description'         => __( 'Posts for Locations', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'show_in_rest' 		  => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'rewrite'             => array( 'slug' => 'location' )
	);

	register_post_type( 'location', $args );

} // custom_post_type__location()

// add_action( 'init', 'custom_post_type__location' );

//////////////////////////////////////////////////////////
////  Custom Post 'Bar'
//////////////////////////////////////////////////////////

function custom_post_type__bar() {

	$labels = array(
		'name'                => _x( 'Bars', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Bar', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Bars', 'text_domain' ),
		'name_admin_bar'      => __( 'Bar', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Bar:', 'text_domain' ),
		'all_items'           => __( 'All Bars', 'text_domain' ),
		'add_new_item'        => __( 'Add New Bar', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'new_item'            => __( 'New Bar', 'text_domain' ),
		'edit_item'           => __( 'Edit Bar', 'text_domain' ),
		'update_item'         => __( 'Update Bar', 'text_domain' ),
		'view_item'           => __( 'View Bar', 'text_domain' ),
		'search_items'        => __( 'Search Bar', 'text_domain' ),
		'not_found'           => __( 'Bar Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Bar Not found in Trash', 'text_domain' ),
	);

	$args = array(
		'label'               => __( 'Bar', 'text_domain' ),
		'description'         => __( 'Posts for Bars', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'show_in_rest' 		  => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'rewrite'             => array( 'slug' => 'bar' )
	);

	register_post_type( 'bar', $args );

} // custom_post_type__bar()

add_action( 'init', 'custom_post_type__bar' );

//////////////////////////////////////////////////////////
////  Custom Post 'Barbershop'
//////////////////////////////////////////////////////////

function custom_post_type__barbershop() {

	$labels = array(
		'name'                => _x( 'Barbershops', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Barbershop', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Barbershops', 'text_domain' ),
		'name_admin_bar'      => __( 'Barbershop', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Barbershop:', 'text_domain' ),
		'all_items'           => __( 'All Barbershops', 'text_domain' ),
		'add_new_item'        => __( 'Add New Barbershop', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'new_item'            => __( 'New Barbershop', 'text_domain' ),
		'edit_item'           => __( 'Edit Barbershop', 'text_domain' ),
		'update_item'         => __( 'Update Barbershop', 'text_domain' ),
		'view_item'           => __( 'View Barbershop', 'text_domain' ),
		'search_items'        => __( 'Search Barbershop', 'text_domain' ),
		'not_found'           => __( 'Barbershop Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Barbershop Not found in Trash', 'text_domain' ),
	);

	$args = array(
		'label'               => __( 'Barbershop', 'text_domain' ),
		'description'         => __( 'Posts for Barbershops', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'show_in_rest' 		  => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'rewrite'             => array( 'slug' => 'barbershop' )
	);

	register_post_type( 'barbershop', $args );

} // custom_post_type__barbershop()

add_action( 'init', 'custom_post_type__barbershop' );

?>
