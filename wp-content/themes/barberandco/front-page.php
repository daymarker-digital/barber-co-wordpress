<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Template Name: Front Page
*	Filename: front-page.php
*
*/

get_header();

$carouselControlsIcon = get_template_directory_uri() . "/img/ui/BARBER-web-content-ui-icon-carousel-nav-next-beige.svg";
$instagramIcon = get_template_directory_uri() . "/img/social/BARBER-web-content-icon-instagram-dark-grey.svg";
 
?>

<div id="front-page" class="page page--front-page" role="main">
							
	<?php if ( have_posts() ) : ?>	
		<?php while ( have_posts() ) : the_post(); ?>
					
		<?php endwhile; ?>
	<?php else : ?>
		<!-- No Post Content -->
	<?php endif; wp_reset_postdata(); ?>	
	
	
	<?php if ( have_rows( 'dynamic_sections' ) ) : $row_count = 1; ?>
	
		<div class="flexible-content flexible-content--front-page">
		
		<?php 
			
			while ( have_rows( 'dynamic_sections' ) ) {
			
				the_row(); 
				$row_layout = get_row_layout(); 
				$section_classes = "section section--colour-theme";
				switch ( $row_layout ) {
					case 'full_width':	
						$section_classes .= " section--full-width";							
						break;
					case 'two_column':
						$section_classes .= " section--two-column";
						break;
					case 'instagram':
						$section_classes .= " section--instagram";
						break;
					default:
						break;
				}
				
				$header_colour_theme = '';
				if ( get_sub_field( 'header_colour_theme' ) ) {
					$header_colour_theme = get_sub_field( 'header_colour_theme' );
					$section_classes .= " section--theme-" . $header_colour_theme;
				}
								
				switch ( $row_layout ) {
					case 'full_width':	
						echo '<div class="' . $section_classes . ' js--vpa-match-viewport-height" data-count="' . $row_count . '" data-theme="' . $header_colour_theme . '">';
						include( locate_template( './includes/front-page/layout--full-width.php' ) );			
						echo '</div>';			
						break;
					case 'two_column':
						echo '<div class="' . $section_classes . ' js--vpa-match-viewport-height" data-count="' . $row_count . '" data-theme="' . $header_colour_theme . '">';
						include( locate_template( './includes/front-page/layout--two-column.php' ) );
						echo '</div>';	
						break;
					case 'instagram':
					
						$account_info = array(
							'access_token_flow' => 'Client-Side (Implicit) Authentication',
							'redirect_uri' => 'https://www.barberandco.com/',
							'user_info' => 'barberandco',
							'user_ID' => '507451900',
							'client_ID' => '5859c095582a4fdfa935d2ee6cad8df9',
							'client_secret' => '5adfdce816c04a98b2848a2196249402',
							'access_token' => '507451900.5859c09.882a8d43f4674c7fb6e4e2e4d21a5242',
							'example_get_url' => 'https://api.instagram.com/v1/users/self/media/recent/?access_token=ACCESS-TOKEN',
							'count' => 1
						);

						$endpoint = 'https://api.instagram.com/v1/users/' . $account_info['user_ID'] . '/media/recent/?access_token=' . $account_info['access_token'] . '&count=' . $account_info['count'];

						//  Initiate curl
						$ch = curl_init();
						// Disable SSL verification
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						// Will return the response, if false it print the response
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						// Set the url
						curl_setopt($ch, CURLOPT_URL,$endpoint);
						// Execute
						$result = curl_exec($ch);
						$result_json = json_decode($result, true);
						// Closing
						curl_close($ch);

						$result_code = $result_json['meta']['code'] ?? "";

						if ( $result_code === 200 ) {
							echo '<div class="' . $section_classes . ' js--vpa-match-viewport-height" data-count="' . $row_count . '" data-theme="' . $header_colour_theme . '">';
							include( locate_template( './includes/front-page/layout--instagram.php' ) );
							echo '</div>';
						}
						
						break;
					default:
						break;
				}
								
				$row_count++;
				
			}
				
		?>
		
		</div>
		
		
		
	<?php endif; ?>
	
</div>
<!-- /#front-page -->

<?php get_footer(); ?>