<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Template Name: Page [ Locations ]
*	Filename: page-locations.php
*
*/

get_header();
 
// ACF Vars
//locations-header-image
$attachment_id = get_field("locations-header-image");
$locations_header_image = wp_get_attachment_image_src( $attachment_id, "full" );
$locations_header_image_title = get_the_title(  $attachment_id  );

?>

<div id="locations" class="page page--locations" role="main">
	
	<?php 
		
		// default data
		$establishment_type = false;
		
		// get data
		if ( get_field( 'establishment_type' ) ) {
			
			$establishment_type = get_field( 'establishment_type' );
			$navigation = array();
			$locations = array();
			
			// WP_Query arguments
			$args = array(
				'post_type'             => array( $establishment_type ),
				'post_status'           => array( 'publish' ),
				'posts_per_page'		=> -1,
				'meta_query'			=> array(
					'location' => array(
			            'key'     => 'city',
			            'compare' => 'EXISTS',
			        )
				),
				'orderby' => array(
					'location' => 'DESC',
					'post_title' => 'ASC',
				)	
			);
	
			// The Query
			$query = new WP_Query( $args );
			
			// The Loop
			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) {
					
					// init data
					$query->the_post();
					
					// default data
					$city = false;
					$location = array(
						'name' => 'Barber & Co.',
						'title' => false,
						'permalink' => false,
						'id' => false,
						'address' => false,
						'address_2' => false,
						'city' => false,
						'region' => false,
						'postal' => false,
						'country' => false,
						'phone' => false,
						'email' => false
					);
					
					// get data
					if ( get_the_title() ) {
						$location['title'] = get_the_title();
					}
					
					if ( get_permalink() ) {
						$location['permalink'] = get_permalink();
					}
					
					if ( get_the_ID() ) {
						$location['id'] = get_the_ID();
					}
					
					if ( get_field( 'city' ) ) {
						$location['city'] = get_field( 'city' );
					}
					
					if ( get_field( 'address' ) ) {
						$location['address'] = get_field( 'address' );
					}
					
					if ( get_field( 'address_2' ) ) {
						$location['address_2'] = get_field( 'address_2' );
					}
					
					if ( get_field( 'region' ) ) {
						$location['region'] = get_field( 'region' );
					}
					
					if ( get_field( 'postal' ) ) {
						$location['postal'] = get_field( 'postal' );
					}
					
					if ( get_field( 'country' ) ) {
						$location['country'] = get_field( 'country' );
					}
					
					if ( get_field( 'phone' ) ) {
						$location['phone'] = get_field( 'phone' );
					}
					
					if ( get_field( 'email' ) ) {
						$location['email'] = get_field( 'email' );
					}
					
					// create 'navigation' data model
					if ( ! in_array( $location['city'], $navigation ) ) {
						array_push( $navigation, $location['city'] );
					}
					
					// create 'navigation' data model
					array_push( $locations, $location );
					
				}
				
			}
			
			//debug_this( $navigation );
			//debug_this( $locations );
			
			// Restore original Post Data
			wp_reset_postdata();	
			
			//////////////////////////////////////////////////////////
			////  Locations Navigation
			//////////////////////////////////////////////////////////
			
			if ( !empty( $navigation ) ) {
				
				echo '<section class="section section--locations-filter">';
					echo '<div class="wrapper"><div class="row"><div class="col-xs-12 col-sm-10 col-sm-offset-1">';
				
						echo '<nav class="locations-filter">';
							echo '<ul class="locations-filter__list">';
						
								foreach ( $navigation as $nav ) {
									echo '<li class="locations-filter__item" data-filter="' . clean_string( $nav ) . '">' . $nav . '</li>';	
								}	
								echo '<li class="locations-filter__item" data-filter="all">All</li>';		
						
							echo '</ul>';
						echo '</nav>';
				
					echo '</div></div></div>';
				echo '</section>';
				echo '<!-- /.section--locations-filter -->';
				
			} // endif
		
			//////////////////////////////////////////////////////////
			////  Locations Content
			//////////////////////////////////////////////////////////
			
			if ( ! empty( $locations ) ) {
				
				echo '<section class="section section--locations">';
					echo '<div class="wrapper"><div class="row"><div class="col-xs-12 col-sm-10 col-sm-offset-1">';
				
						echo '<div class="locations">';
							echo '<div class="locations__grid row">';
							
								echo '<div class="locations__sizr col-xs-12 col-md-6"></div>';
							
								$count = 0;
						
								foreach ( $locations as $location ) {
									
									// default data
									$title = $permalink = $id = $city = false;
									$address = $address_2 = $region = $postal = $country = false;
									$featured_image = $featured_image_xs = $featured_image_id = false;
									$phone = $email = false;
									$address_display = $address_directions = '';
									$google_maps_search = 'https://maps.google.com/?q=';
									
									// get post data
									if ( isset( $location['title'] ) && !empty( $location['title'] ) ) {
										$title = $location['title'];
									}
									if ( isset( $location['permalink'] ) && !empty( $location['permalink'] ) ) {
										$permalink = $location['permalink'];
									}
									if ( isset( $location['id'] ) && !empty( $location['id'] ) ) {
										$id = $location['id'];
									}
									if ( get_post_thumbnail_id( $id ) ) {
																				
										$featured_image_id = get_post_thumbnail_id( $id );
										$featured_image = wp_get_attachment_image_src( $featured_image_id, 'desktop-large' );
										$featured_image = $featured_image[0];
										$featured_image_xs = wp_get_attachment_image_src( $featured_image_id, 'lazyload-tiny' );
										$featured_image_xs = $featured_image_xs[0];
																				
									} else {
										
										if ( have_rows( 'gallery', $id ) ) {
											while ( have_rows( 'gallery', $id ) ) {
												
												if ( get_row_index() > 0 ) {
													
													// exit loop
													break;
													
												} else {
													
													// init data
													the_row();
													
													// get data
													if ( get_sub_field( 'image' ) ) {
														$featured_image = get_sub_field( 'image' );
														$featured_image_xs = $featured_image['sizes']['lazyload-tiny'];
														$featured_image = $featured_image['sizes']['desktop-large'];
													}
													
												}
												
											} // endwhile
										} // endif
										
									}
									
									// get address data
									if ( isset( $location['address'] ) && !empty( $location['address'] ) ) {
										$address = $location['address'];
										$address_display = $address;
										$address_directions = $address;
									}
									
									if ( isset( $location['address_2'] ) && !empty( $location['address_2'] ) ) {
										$address_2 = $location['address_2'];
										$address_display = $address_2 . '–' . $address_display;
										if ( empty( $address_directions ) ) { 
											$address_directions	= $address_2;
										} else {
											$address_directions = $address_2 . '–' . $address_directions;
										}
									}
									
									if ( isset( $location['city'] ) && !empty( $location['city'] ) ) {
										$city = $location['city'];
										if ( empty( $address_directions ) ) { 
											$address_directions	= $city;
										} else {
											$address_directions .= ', ' . $city; 
										}
									}
									
									if ( isset( $location['region'] ) && !empty( $location['region'] ) ) {
										$region = $location['region'];
										if ( empty( $address_directions ) ) { 
											$address_directions	= $region;
										} else {
											$address_directions .= ', ' . $region; 
										}
									}
		
									if ( isset( $location['postal'] ) && !empty( $location['postal'] ) ) {
										$postal = $location['postal'];
										if ( empty( $address_directions ) ) { 
											$address_directions	= $postal;
										} else {
											$address_directions .= ', ' . $postal; 
										}
									}
									
									if ( isset( $location['country'] ) && !empty( $location['country'] ) ) {
										$country = $location['country'];
										if ( empty( $address_directions ) ) { 
											$address_directions	= $country;
										} else {
											$address_directions .= ', ' . $country; 
										}
									}
									
									if ( isset( $location['phone'] ) && !empty( $location['phone'] ) ) {
										$phone = $location['phone'];
									}
									
									if ( isset( $location['email'] ) && !empty( $location['email'] ) ) {
										$email = $location['email'];
									}
																
									echo '<div class="locations__item col-xs-12 col-md-6 '. clean_string( $city ). '" data-city="' . clean_string( $city ) . '" data-count="' . $count . '">';
									
										echo '<div class="locations__image lazyload-container lazyload-container--background-image">';
											if ( $featured_image ) {
												echo '<div class="lazyload--background-image lazyload" data-bg="' . $featured_image . '" style="background-image: url(' . $featured_image_xs . ');"></div>';
											} else {
												echo '<div class="no-image"></div>';
											}
										echo '</div>';
										echo '<!-- /.locations__image -->';
										
										echo '<div class="locations__info">';
										
											echo '<div class="locations__name">';
												if ( $permalink ) {
													echo '<a href="' . $permalink . '">';
												}
												if ( $title ) {
													echo '<h2 class="locations__title">' . $title . '</h2>';
												}
												if ( $city ) {
													echo '<h3 class="locations__city">' . $city . '</h3>';
												}
												if ( $permalink ) {
													echo '</a>';
												}
											echo '</div>';
											echo '<!-- /.locations__name -->';
											
											echo '<div class="locations__contact">';
												if ( $address_display || $phone || $email ) {
													echo '<ul>';	
														if ( $address_display ) {
															echo '<li><a href="' . google_maps_link_builder( $location ) . '" target="_blank">' . $address_display . '</a></li>';
														}
														if ( $phone ) {
															echo '<li><a href="tel:' . $phone . '">' . $phone . '</a></li>';
														}
														if ( $email ) {
															echo '<li><a href="mailto:' . $email . '">' . $email . '</a></li>';
														}
													echo '</ul>';
												}
											echo '</div>';
											echo '<!-- /.locations__contact -->';
										
										echo '</div>';
										echo '<!-- /.locations__info -->';
																			
									echo '</div>';	
									
									$count++;
																	
								}					
						
							echo '</div>';
							echo '<!-- /.locations__grid -->';
						echo '</div>';
						echo '<!-- /.locations-->';
						
					echo '</div></div></div>';
				echo '</section>';
				echo '<!-- /.section--regions -->';	
				
			} // endif
			
		}
	
	?>
	
</div>
<!-- /#locations -->

<?php get_footer(); ?>