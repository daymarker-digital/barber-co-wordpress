//////////////////////////////////////////////////////////
////  Locations
//////////////////////////////////////////////////////////

var Locations = (function () {
	
	var debugThis = false;
	var info = { name : 'Locations', version : 1.0 };	
	
	//////////////////////////////////////////////////////////
	////  Private Methods
	//////////////////////////////////////////////////////////
		
	var _setPosition = function() {
          	
        var viewportWidth = window.innerWidth;  	
        var referenceHeight;  	
		var matchParent, matchChild;
		var matchContainer = $(".js-match--container");
		matchParent = matchContainer.find(".js-match--parent");
		matchChild = matchContainer.find(".js-match--child");
		
		if ( viewportWidth >= 992 ) {
                    
			referenceHeight = matchParent.outerHeight();                
			matchChild.css({
				"height": referenceHeight + "px"
            });

		} else {
			matchChild.css({
				"height":"auto"
            });
        }
                
    };

    //////////////////////////////////////////////////////////
	////  Public Methods
	//////////////////////////////////////////////////////////
	
	var matchParentHeight = function() {
      
      	_setPosition();
      
      	new ResizeSensor( $('#carousel-container--location-gallery'), function() {
           _setPosition();
        });
      
    };
    
    var tabsControllers = function() {
  	
		$(".tabs-navigation-item").on("click", function(event){
                    
			$(".tabs-navigation-item").removeClass("active");
          
			$(this).addClass("active");
      		
			var index = $(this).index();
          
			$(".tabs-content li").hide();
          	
			$(".tabs-content li").eq( index ).fadeIn(500);
          
		});
      
    };  
    
    //////////////////////////////////////////////////////////
	////  Public | Filter Locations
	//////////////////////////////////////////////////////////
    
    var filterLocations = function() {
	    
	    var $grid = $('.locations__grid').isotope({
			itemSelector: '.locations__item',
			percentPosition: true,
			masonry: {
				// use element for option
			    columnWidth: '.locations__sizr'
			}
		});

		function onArrange() {
			console.log('ARRANGED');
			$('[role="main"]').css({ 'overflow' : 'initial' });
		}
		
		// bind event listener
		$grid.on( 'arrangeComplete', onArrange );
		
		/*
		// un-bind event listener
		$grid.off( 'arrangeComplete', onArrange );
		
		// bind event listener to be triggered just once. note ONE not ON
		$grid.one( 'arrangeComplete', function() {
		  console.log('arrange done, just this one time');
		});
		*/	    
      
    	$('.locations-filter__item').on('click', function(event){
	    	
	    	/*
	    	// filter .metal items
			$grid.isotope({ filter: '.metal' });
			
			// filter .alkali OR .alkaline-earth items
			$grid.isotope({ filter: '.alkali, .alkaline-earth' });
			
			// filter .metal AND .transition items
			$grid.isotope({ filter: '.metal.transition' });
			
			// show all items
			$grid.isotope({ filter: '*' });
			*/
	    	
	    	var thisEl = $(this);
	    	
	    	// toggle overflow
	    	$('[role="main"]').css({ 'overflow' : 'hidden' });
	    	
	    	// filter
	    	var thisFilter = thisEl.attr('data-filter');
	    	
	    	if ( 'all' == thisFilter ) {
		    	
		    	// $('.locations__item').addClass('active');
		    	$grid.isotope({ filter: '*' });
		    	
	    	} else {
		    	
		    	$grid.isotope({ filter: '.' + thisFilter });
		    			    	
	    	}
	    	
	    	// update active state
	    	thisEl.closest('ul').find('.locations-filter__item').removeClass('active');
	    	thisEl.addClass('active');
	    	
    	});
      
	};

	//////////////////////////////////////////////////////////
	////  Public Methods
	//////////////////////////////////////////////////////////
			
	var init = function( $params ) {
      
		Debug.log( debugThis, info, 'start' );
		
		filterLocations();
      	
		if ( $("#location").length ) {
      
			matchParentHeight();
			
			tabsControllers();
 
		}
     
		Debug.log( debugThis, info, 'stop' );	
      
	};
	
	//////////////////////////////////////////////////////////
	////  Returned
	//////////////////////////////////////////////////////////
	
	return {
      	info : info,
		init : init
	};
	
})();