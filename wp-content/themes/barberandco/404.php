<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Template Name: Page [ 404 ]
*	Filename: 404.php
*
*/

get_header();
 
?>

<div id="error-404" class="page page--error-404" role="main">
	
	<div class="js--vpa-match-viewport-height">
		
		<div class="error-message error-message--404">
		
			<div class="wrapper"><div class="row"><div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
						
				<h1 class="headline headline--error">Error Code: 404</h1>
				<p>The page or content you were looking for is not here.<br> Let us take you <a href="<?php echo get_home_url(); ?>">home</a>.</p>
						
			</div></div></div>
			<!-- /.wrapper .row .col -->
				
		</div>
		
	</div>
			
		
</div>
<!-- #page.error-404 -->

<?php get_footer(); ?>