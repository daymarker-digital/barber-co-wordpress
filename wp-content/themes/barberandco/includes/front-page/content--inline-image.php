<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Filename: content--inline-image.php
*
*/

// Default Vars
$image = $image_src = $bg_colour = false;

// message vars
$message_obj = array();
$message_obj['colour_theme'] = get_sub_field( 'colour_theme' );
$message_obj['message'] = get_sub_field( 'message' );
$message_obj['cta'] = get_sub_field( 'cta' );
$message_obj['link'] = get_sub_field( 'link' );
$message_obj['pos_hor'] = get_sub_field( 'pos_hor' );
$message_obj['pos_vert'] = get_sub_field( 'pos_vert' );

if ( get_sub_field( 'background_colour' ) ) {
	$bg_colour = get_sub_field( 'background_colour' );
} 

// Get Values
if ( get_sub_field( 'image' ) ) {

	$image = get_sub_field( 'image' );
	
	$image_src = $image['url'];
	$image_alt = $image['alt'];
	
	echo '<div class="inline-image">';
	
	if ( $bg_colour ) {
		echo '<div class="background-colour" style="background:' . $bg_colour . ';"></div>';
		echo '<!-- /.background-colour -->';
	}
	
	if ( $image_alt ) {
		echo '<img src="' . $image_src . '" alt="' . $image_alt . '" />';
	} else {
		echo '<img src="' . $image_src . '" alt="Barber & Co Image" />';
	}
		
	echo '</div>';
	echo '<!-- /.inline-image -->';
	
	include( locate_template( './includes/front-page/content--message.php' ) );
	
} else {
	echo '<!-- No Image -->';
}

?>