<?php
	
/*
*	
*	Theme: Daymarker Skeleton Theme
*	Filename: loop-custom-post.php
*
*/

// Default WP Query Vars
$debugPosts = false;
$customPostType = array( "event" );
$customPostStatus = array( "publish" );
$paginateQuery = true;
$customPostCount = -1;
$excludePostsWithCategoryID = array( 6969 );
$order = "ASC";

// Set WP Query Pagination Rules	
if ( get_query_var('paged') ) :
	$paged = get_query_var('paged'); 
elseif ( get_query_var('page') ) :
	$paged = get_query_var('page'); 
else :
	$paged = 1;
endif;
																
// Set Date (today)
$todaysDate = current_time( 'Ymd' );
			
// Set Time (today)
$todaysTime = new DateTime;
$todaysTime = $todaysTime->setTime(0, 0);
$todaysTime = $todaysTime->format('H:i:s');
	
// WP Query Args 	
$args = array (
	'post_type'         =>	$customPostType,
	'post_status'       =>	$customPostStatus,
	'pagination'        =>	$paginateQuery,
	'posts_per_page'	=>	$customPostCount,
	'category__not_in' 	=>  $excludePostsWithCategoryID,
	'paged'				=>	$paged,
	'meta_query'		=> 	array(
		'day' 				=> array( 
			'key'				=> 'eventDate',
			'compare' 			=> '>=',
			'value'				=> $todaysDate,
			'type' 				=> 'DATETIME'
		),
		'startTime' 			=> array(
			'key'				=> 'eventStartTime',
			'compare' 			=> 'EXISTS'
		)
	),										
	'orderby'				=> 	array(
		'day'				=> 	$order,
		'startTime'			=> 	$order
	)
);
																	
$customQuery = new WP_Query( $args );
	
?>
										
<?php if ( $customQuery->have_posts() ) : ?>

	<ul id="custom-post-results-list" class="post-result-list custom-post-results-list">
		
		<?php while ( $customQuery->have_posts() ) : $customQuery>the_post(); ?>
			
			<li class="custom-post-results-item">
			
				<?php if ( $debugPosts ) : ?>
			
						<?php debugObject( $post ); ?>	
						<?php debugObject( get_post_meta( $post->ID ) ); ?>
					
				<?php else: ?>
				
					<article id="post-<?php echo clean_string( get_the_title() ); ?>">
						<a href="<?php the_permalink(); ?>">
							
							<div class="article__header">	
								<?php if ( get_the_title() ) : ?>				
									<h2 class="headline headline--beta"><?php the_title(); ?></h2>
								<?php endif; ?>
							</div>
							<!-- /.article__header -->
								
							<div class="article__main">
								<?php if ( get_the_excerpt() ) : ?>
									<div class="exceprt rte">
										<?php the_excerpt(); ?>	
									</div>
								<?php endif; ?>
							</div>
							<!-- /.article__main -->
								
							<div class="article__footer"></div>
							<!-- /.article__footer -->
											
						</a>
					</article>
					<!-- /#event-<?php echo daymarker_clean_string( get_the_title() ); ?> -->
					
				<?php endif; ?>
			
			</li>
			<!-- /.custom-post-results-item -->
			
		<?php endwhile; ?>
		
	</ul>
	<!-- /#custom-post-results-list -->

<?php else : ?>
	<!-- NO 'EVENT' POSTS !!! -->		
<?php endif;  wp_reset_postdata(); ?>