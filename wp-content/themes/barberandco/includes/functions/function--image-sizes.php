<?php
	
//////////////////////////////////////////////////////////
////  Custom Image Sizes
//////////////////////////////////////////////////////////

function daymarker_image_sizes() {

	// TRUE designates cropping of images
	// Simply two numbers, ie (588, 331) designates scale based on width
	// 'Soft Crop' scales image down to dimensions
	// 'Hard Crop' crops image down to dimensions without scale 
	// Examples
	// add_image_size( 'custom-size', 220, 180 ); // 220 pixels wide by 180 pixels tall, soft crop / scaled
	// add_image_size( 'custom-size', 220, 180, true ); // 220 pixels wide by 180 pixels tall, hard crop mode
	// add_image_size( 'custom-size', 220, 220, array( 'left', 'top' ) ); // 220 pixels wide by 180 pixels tall, hard cropped from left top
	// add_image_size( 'singlepost-thumb', 590, 9999 ); // // 590 pixels wide, no limit on height

	// required 
	add_theme_support( 'post-thumbnails' );
	
	//add_image_size( 'viewport-extra-small', 560, 9999 ); // // 590 pixels wide, no limit on height
	//add_image_size( 'viewport-small', 768, 9999 ); // // 590 pixels wide, no limit on height
	//add_image_size( 'viewport-medium', 960, 9999 ); // // 590 pixels wide, no limit on height
	//add_image_size( 'viewport-large', 1160, 9999 ); // // 590 pixels wide, no limit on height
	//add_image_size( 'viewport-extra-large', 1360, 9999 ); // // 590 pixels wide, no limit on height
	//add_image_size( 'viewport-extra-extra-large', 1560, 9999 ); // // 590 pixels wide, no limit on height
	
	// 1:1 aspect ratio
	//add_image_size( 'square-mobile-extra-small', 450, 450 ); // 480 pixels wide by 240 pixels tall, soft crop / scaled
	//add_image_size( 'square-mobile-small', 780, 780 ); // 780 pixels wide by 585 pixels tall, soft crop / scaled
	//add_image_size( 'square-desktop-medium', 900, 900 ); // 1200 pixels wide by 900 pixels tall, soft crop / scaled
	//add_image_size( 'square-desktop-large', 1100, 1100 ); // 1400 pixels wide by 1050 pixels tall, soft crop / scaled
	
	// 4:3 aspect ratio
	//add_image_size( 'wide-mobile-extra-small', 480, 240 ); // 480 pixels wide by 240 pixels tall, soft crop / scaled
	//add_image_size( 'wide-mobile-small', 780, 585 ); // 780 pixels wide by 585 pixels tall, soft crop / scaled
	//add_image_size( 'wide-desktop-medium', 1200, 900 ); // 1200 pixels wide by 900 pixels tall, soft crop / scaled
	//add_image_size( 'wide-desktop-large', 1400, 1050 ); // 1400 pixels wide by 1050 pixels tall, soft crop / scaled
	
	// 16:10 aspect ratio
	//add_image_size( 'super-wide-mobile-extra-small', 480, 300 ); // 480 pixels wide by 300 pixels tall, soft crop / scaled
	//add_image_size( 'super-wide-mobile-small', 780, 488 ); // 780 pixels wide by 488 pixels tall, soft crop / scaled
	//add_image_size( 'super-wide-desktop-medium', 1200, 750 ); // 1200 pixels wide by 750 pixels tall, soft crop / scaled
	//add_image_size( 'super-wide-desktop-large', 1400, 875 ); // 1400 pixels wide by 875 pixels tall, soft crop / scaled
	
	add_image_size( 'lazyload-tiny', 1, 9999 ); // 10 pixels wide, no limit on height
	add_image_size( 'lazyload-small', 10, 9999 ); // 10 pixels wide, no limit on height
	add_image_size( 'phone-small', 400, 9999 ); // 1200 pixels wide, no limit on height
	add_image_size( 'phone-large', 600, 9999 ); // 1400 pixels wide, no limit on height
	add_image_size( 'tablet-small', 800, 9999 ); // 1200 pixels wide, no limit on height
	add_image_size( 'tablet-large', 1000, 9999 ); // 1400 pixels wide, no limit on height
	add_image_size( 'desktop-small', 1200, 9999 ); // 1200 pixels wide, no limit on height
	add_image_size( 'desktop-large', 1400, 9999 ); // 1400 pixels wide, no limit on height
	add_image_size( 'desktop-extra-large', 1600, 9999 ); // 1650 pixels wide, no limit on height
			
} // daymarker_image_sizes()

add_action( 'after_setup_theme', 'daymarker_image_sizes' );

?>