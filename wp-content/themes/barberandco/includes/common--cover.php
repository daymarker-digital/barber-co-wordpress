<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Filename: common--cover.php
*
*/

// Theme Vars
$home = Daymarker( 'home_url' );
$template_dir = Daymarker( 'template_dir' );
$is_production = Daymarker( 'production' );
$is_maintenance = Daymarker( 'maintenance' );
$theme_classes = Daymarker( 'theme_classes' );

$image_src = $headline = false;
if ( get_field( "cover-image" ) ) {
	$image_src = get_field( "cover-image" );
	$image_src = $image_src['url'];
}
if ( get_field( "cover-headline" ) ) {
	$headline = get_field( "cover-headline" );
}

?>
							
<div id="cover" class="cover">
	
	<div class="cover__background-image lazyload-container lazyload-container--background-image">			
		<div class="lazyload lazyload--background-image" <?php if ( $image_src ) : ?>data-bg="<?php echo $image_src; ?>"<?php endif; ?>></div>
	</div>
	<!-- /.cover__background-image -->
	
	<?php if ( $headline ) : ?>
	
		<div class="cover__overlay"></div>
		<!-- /.cover__overlay -->
		
		<div class="cover__content">
			<div class="wrapper"><div class="row"><div class="col-xs-12">
				<h1><?php echo $headline; ?></h1>
			</div></div></div>
			<!-- /.wrapper .row .col -->
		</div>
		<!-- /.cover__content -->
		
	<?php endif; ?>
	
</div>
<!-- /#cover -->