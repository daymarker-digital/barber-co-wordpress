<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Filename: content--message.php
*
*/

// Default Vars
$content_classes = "content";
$content = $link = $message = $cta = $colour_theme = false;

// Get Values
if ( isset( $message_obj ) && !empty( $message_obj ) ) {
	
	$content = $message_obj;
	
	if ( isset( $content['message'] ) && !empty( $content['message'] ) ) {
		$message = $content['message'];
	}
	if ( isset( $content['cta'] ) && !empty( $content['cta'] ) ) {
		$cta = $content['cta'];
	}
	if ( isset( $content['link'] ) && !empty( $content['link'] ) ) {
		$link = $content['link'];
	}
	if ( isset( $content['pos_hor'] ) && !empty( $content['pos_hor'] ) ) {
		$pos_hor = $content['pos_hor'];
		$content_classes .= ' content--' . $pos_hor;
	}
	if ( isset( $content['pos_vert'] ) && !empty( $content['pos_vert'] ) ) {
		$pos_hor = $content['pos_vert'];
		$content_classes .= ' content--' . $pos_hor;
	}
	if ( isset( $content['colour_theme'] ) && !empty( $content['colour_theme'] ) ) {
		$colour_theme = $content['colour_theme'];
		$content_classes .= ' content--' . $colour_theme;
	}
	
	if ( $message || $cta ) {
	
		// Print Content
		echo '<div class="' . $content_classes . '">';
		if ( $link ) {
			echo '<a href="' . $link . '">';
		}
		if ( $message ) {
			echo '<span class="content__message">' . $message . '</span>';
		}
		if ( $cta ) {
			echo '<span class="content__cta">' . $cta . '</span>';
		}
		if ( $link ) {
			echo '</a>';
		}
		echo '</div>';
		echo '<!-- /.content -->';
		
	}
	
} else {
	echo '<!-- No Message Content -->';
}

?>