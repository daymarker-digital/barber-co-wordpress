<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Template Name: Page [ Experience ]
*	Filename: page-experience.php
*
*/

get_header();
// ACF Vars

?>

<div id="experience" class="page page--experience" role="main">
	
	<?php if ( get_field( "cover-image" ) ) : ?>
		<!-- Cover Image -->
		<?php include( locate_template( "./includes/common--cover.php" ) ); ?>
		<!-- End of Cover Image -->
	<?php endif; ?>
		
	<!-- Origins -->	
	<section class="section section--origins">
			
		<div class="wrapper"><div class="row">
			
			<div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-2">
										
				<div class="row inner">
					
					<div class="origins">
					
						<div class="col-md-8">
							<?php if ( get_field( "origins-headline" ) || get_field( "origins-details" ) ) : ?>
								<div class="origins-info">
									<?php if ( get_field( "origins-headline" ) ) : ?>
										<h2 class="headline headline--section-title"><?php the_field( "origins-headline" ); ?></h2>
									<?php endif; ?>
									<?php if ( get_field( "origins-details" ) ) : ?>
										<div class="details details--rte"><?php the_field( "origins-details" ); ?></div>
									<?php endif; ?>
								</div>
								<!-- /.origins-info -->
							<?php endif; ?>
						</div>
						<!-- /.col -->
					
						<div class="col-md-10">
							<?php if ( get_field( "origins-image-large" ) || get_field( "origins-image-small" ) ) : ?>
								<div class="origins-images">
									
									<?php 
										$image = $image_src = $image_alt = false;
										if ( get_field( "origins-image-large" ) ) {
											$image = get_field( "origins-image-large" );
											$image_src = $image['url'];
											$image_alt = $image['alt'];
										}
									?>
									<div class="images image--large lazyload-container lazyload-container--image">
										<img class="lazyload lazyload--image" data-src="<?php echo $image_src; ?>" alt="<?php echo $image_alt; ?>" />
									</div>
									<!-- /.image--large -->
									
									<?php 
										$image = $image_src = $image_alt = false;
										if ( get_field( "origins-image-small" ) ) {
											$image = get_field( "origins-image-small" );
											$image_src = $image['url'];
											$image_alt = $image['alt'];
										}
									?>
									<div class="images image--small lazyload-container lazyload-container--image">
										<img class="lazyload lazyload--image" data-src="<?php echo $image_src; ?>" alt="<?php echo $image_alt; ?>" />
									</div>
									<!-- /.image--small -->

								</div>
								<!-- /.origins-images -->
							<?php endif; ?>
						</div>
						<!-- /.col -->
					
					</div>
					<!-- /.origins -->
					
				</div>
				<!-- /.row.inner -->
	
			</div>
			<!-- /.col -->
		</div></div>
		<!-- /.wrapper .row -->
			
	</section>	
	<!-- End of Origins -->
	
	<!-- Founders -->
	<section class="section section--founders">
		
		<div class="wrapper"><div class="row">
			
			<div class="col-xs-10 col-xs-offset-1">
														
				<div class="row inner">
					
					<div class="history">
						
						<div class="col-md-5 col-md-offset-1">
														
							<?php if ( get_field( "history-headline" ) ) : ?>	
								<h2 class="headline headline--section-title"><?php the_field( "history-headline" ); ?></h2>
							<?php endif; ?>
							
							<?php if ( get_field( "history-message" ) ) : ?>
								<div class="message message--rte">
									<?php the_field( "history-message" ); ?>
								</div>
							<?php endif; ?>
								
						</div>
						<!-- /.col -->
							
						<div class="col-md-6">
							
							<?php if ( get_field( "history-image-wide" ) || get_field( "history-image-sqaure" )  ) : ?>
							<div class="origins-images origins-images--history">
								
								<?php 
									$image = $image_src = $image_alt = false;
									if ( get_field( "history-image-square" ) ) {
										$image = get_field( "history-image-square" );
										$image_src = $image['url'];
										$image_alt = $image['alt'];
									}
								?>
								<div class="images image--square lazyload-container lazyload-container--image">
									<img class="lazyload lazyload--image" data-src="<?php echo $image_src; ?>" alt="<?php echo $image_alt; ?>" />
								</div>
								<!-- /.image--square -->
								
								<?php 
									$image = $image_src = $image_alt = false;
									if ( get_field( "history-image-wide" ) ) {
										$image = get_field( "history-image-wide" );
										$image_src = $image['url'];
										$image_alt = $image['alt'];
									}
								?>
								<div class="images image--wide lazyload-container lazyload-container--image">
									<img class="lazyload lazyload--image" data-src="<?php echo $image_src; ?>" alt="<?php echo $image_alt; ?>" />
								</div>
								<!-- /.image--wide -->
						
							</div>
							<!-- /.origins-images--history -->
							<?php endif; ?>
							
						</div>
						<!-- /.col -->
						
					</div>
					<!-- /.history -->
					
					<div class="process">
						
						<div class="col-md-6 col-md-push-6">
							
							<?php if ( get_field( "process-headline" ) ) : ?>	
								<h2 class="headline headline--section-title"><?php the_field( "process-headline" ); ?></h2>
							<?php endif; ?>
							
							<?php if ( get_field( "process-message" ) ) : ?>
								<div class="message message--rte">
									<?php the_field( "process-message" ); ?>
								</div>
							<?php endif; ?>
							
						</div>
						
						<div class="col-md-6 col-md-pull-6">
							
							<?php if ( get_field( "process-image-wide" ) || get_field( "process-image-square" )  ) : ?>
							<div class="origins-images origins-images--process">
								
								<?php 
									$image = $image_src = $image_alt = false;
									if ( get_field( "process-image-square" ) ) {
										$image = get_field( "process-image-square" );
										$image_src = $image['url'];
										$image_alt = $image['alt'];
									}
								?>
								<div class="images image--square lazyload-container lazyload-container--image">
									<img class="lazyload lazyload--image" data-src="<?php echo $image_src; ?>" alt="<?php echo $image_alt; ?>" />
								</div>
								<!-- /.image--square -->
								
								<?php 
									$image = $image_src = $image_alt = false;
									if ( get_field( "process-image-wide" ) ) {
										$image = get_field( "process-image-wide" );
										$image_src = $image['url'];
										$image_alt = $image['alt'];
									}
								?>
								<div class="images image--wide lazyload-container lazyload-container--image">
									<img class="lazyload lazyload--image" data-src="<?php echo $image_src; ?>" alt="<?php echo $image_alt; ?>" />
								</div>
								<!-- /.image--wide -->
														
							</div>
							<!-- /.origins-images--process -->
							<?php endif; ?>
							
						</div>
						<!-- /.col -->
						
					</div>
					<!-- /.process -->
					
				</div>
				<!-- /.row.inner -->
													
			</div>
			<!-- /.col -->
			
		</div></div>
		<!-- /.wrapper .row -->
		
	</section>
	<!-- End of Founders -->
	
</div>
<!-- /#experience -->

<?php get_footer(); ?>