<?php

//////////////////////////////////////////////////////////
////  Push Content | Right
//////////////////////////////////////////////////////////

$home = Daymarker('home_url');
$shopify_url = Daymarker('shopify_url');
$template_dir = Daymarker('template_dir');
$is_production = Daymarker('production');
$is_booktenance = Daymarker('booktenance');
$theme_classes = Daymarker('theme_classes');

?>

<div class="push-content push-content--overlay push-content--right push-content--overlay-right push-content--booking-locations" id="push-content--overlay-right">
  <nav class="menu menu--push menu--book">
    <ul class="menu__list menu__list--push menu__list--book">
      <li class="menu__item menu__item--heading">Vancouver</li>
      <li class="menu__item menu__item--internal">
        <a href="https://online.getsquire.com/book/barber-and-co-main-street-vancouver/barber">Main Street</a>
      </li>
      <li class="menu__item menu__item--internal">
        <a href="https://online.getsquire.com/book/barber-and-co-yaletown-vancouver/barber">Yaletown</a>
      </li>
      <li class="menu__item menu__item--heading">Toronto</li>
      <li class="menu__item menu__item--internal">
        <a href="https://online.getsquire.com/book/barber-and-co-ossington-toronto/barber">Ossington</a>
      </li>
    </ul>
  </nav>
</div>

