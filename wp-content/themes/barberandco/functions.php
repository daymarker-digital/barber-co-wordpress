<?php

/*
*
*	Theme: Barber & Co. WordPress Theme
*	Filename: functions.php
*
*/

//////////////////////////////////////////////////////////
////  Daymarker Digital Function
//////////////////////////////////////////////////////////

function Daymarker ( $params ) {

	global $post;

	if ( ! empty ( $params ) ) {

		switch ( $params ) {
			case 'production':
				return true;
				break;
			case 'maintenance':
				return false;
				break;
			case 'home_url':
				return get_home_url();
				break;
			case 'shopify_url':
				return 'https://shop.barberandco.ca';
				break;
			case 'template_dir':
				return get_template_directory_uri();
				break;
			case 'post_ID':
				return get_the_ID();
				break;
			case 'post_type':
				return get_post_type( get_the_ID() );
				break;
			case 'current_template':
				return basename( get_page_template(), ".php" );
				break;
			case 'wp_version':
				return get_bloginfo( "version" );
				break;
			case 'php_version':
				return phpversion();
				break;
			case 'theme_classes':
				return get_theme_classes();
				break;
		}

	}

}

add_action( 'init', 'Daymarker', 0 );

//////////////////////////////////////////////////////////
////  Utilities
//////////////////////////////////////////////////////////

include_once( 'includes/functions/function--utilities.php' );

//////////////////////////////////////////////////////////
////  Security
//////////////////////////////////////////////////////////

include_once( 'includes/functions/function--security.php' );

//////////////////////////////////////////////////////////
////  ACF Master Options
//////////////////////////////////////////////////////////

include_once( 'includes/functions/function--advanced-custom-fields.php' );

//////////////////////////////////////////////////////////
////  ACF Master Options
//////////////////////////////////////////////////////////

include_once( 'includes/functions/function--google-maps-link-builder.php' );

//////////////////////////////////////////////////////////
////  Menus
//////////////////////////////////////////////////////////

include_once( 'includes/functions/function--menus.php' );

//////////////////////////////////////////////////////////
////  Image Sizes
//////////////////////////////////////////////////////////

include_once( 'includes/functions/function--image-sizes.php' );

//////////////////////////////////////////////////////////
////  Custom Post Types
//////////////////////////////////////////////////////////

include_once( 'includes/functions/function--custom-post-types.php' );

//////////////////////////////////////////////////////////
////  Enqueue Scripts & Styles
//////////////////////////////////////////////////////////

include_once( 'includes/functions/function--enqueue-scripts-styles.php' );
