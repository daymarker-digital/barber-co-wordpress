<?php
/**
 * The template for displaying Search Results pages.
 *
 * 
 * 
 */
 
get_header(); 

?>

<div class="wrapper">
		<div class="row">
				<div class="col-sm-12">
					
				<?php get_search_form(); ?>
 
	            <?php if ( have_posts() ) : ?>
	 
	                	<header class="page-header">
	                    		<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'shape' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
	                	</header><!-- .page-header -->
	                	
						<?php while ( have_posts() ) : the_post(); 
							
							// ACF Vars
							$text_field = get_field("text-field");
							$textarea_field = get_field("text-area");
							$email_field = get_field("email");
							$wysiwyg_field = get_field("wysiwyg");
							$attachment_id = get_field("image");
							$size = "mobile-full"; // (thumbnail, medium, large, full or custom size)
							$image = wp_get_attachment_image_src( $attachment_id, $size );
							
						?>
						
						<article>
							
								<a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a> 
								<h1><?php echo $text_field; ?></h1>
								<p><?php echo $textarea_field; ?></p>
								<p><?php echo $email_field; ?></p>
								<div class="rte">
									 	<?php echo ddc_custom_field_excerpt( "wysiwyg" , 100); ?>							
								</div>
								<!-- /.rte -->
							
						</article>
							 
						<?php endwhile; ?>
	 
				<?php else : ?>
	 
						<h2>Sorry, but nothing matched your search terms. Please try again with some different keywords.</h2>
						<?php get_search_form(); ?>
	 
				<?php endif; ?>
         
				</div>
				<!-- /.col-sm-12 -->
		</div>
		<!-- /.row -->
</div>
<!-- /.wrapper -->
 
<?php get_footer(); ?>