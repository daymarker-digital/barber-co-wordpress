<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Template Name: Page [ Academy ]
*	Filename: page-academy.php
*
*/

get_header();
 
// ACF Vars
$apprenticeship_headline = get_field("apprenticeship-headline");
$apprenticeship_details = get_field("apprenticeship-details");

//apprenticeship-information-book-link-title
$file_link_name = get_field("apprenticeship-information-book-link-title");

$file = get_field("apprenticeship-information-book");
$apprenticeship_information_book = wp_get_attachment_url( $file );

$attachment_id = get_field("apprenticeship-image-large");	
$apprenticeship_image_large = wp_get_attachment_image_src( $attachment_id, "full" );
$apprenticeship_image_large_title = get_the_title(  $attachment_id  );

$attachment_id = get_field("apprenticeship-image-small");	
$apprenticeship_image_small = wp_get_attachment_image_src( $attachment_id, "full" );
$apprenticeship_image_small_title = get_the_title(  $attachment_id  );

$workshop_headline = get_field("workshop-headline");
$workshop_details = get_field("workshop-details");

?>

<div id="academy" class="page page--academy" role="main">
	
	<?php if ( get_field( "cover-image" ) ) : ?>
		<!-- Cover Image -->
		<?php include( locate_template( "./includes/common--cover.php" ) ); ?>
		<!-- End of Cover Image -->
	<?php endif; ?>
		
	<section class="section default apprenticeship">
			
		<div class="wrapper"><div class="row">
			
			<div class="col-md-7 col-md-offset-1">
				
				<div class="content apprenticeship">
					<h2 class="headline headline--section-title"><?php echo $apprenticeship_headline; ?></h2>
					<div class="details rte"><?php echo $apprenticeship_details; ?></div>
				</div>
				<!-- /.content.apprenticeship -->
			
			</div>
			<!-- /.col -->
			
			<div class="col-md-3">
				<div class="apprenticeship-sidebar">
					<p>Talk to us About</p>
					<a class="button button--inline button--call-to-action" href="mailto:academy@barberandco.ca?subject=Let's%20Talk%20About%20Group%20Bookings">Group Bookings</a>
				</div>
				<!-- /.apprenticeship-sidebar -->
			</div>
			<!-- /.col -->
		
						
			<div class="col-md-10 col-md-offset-1">
				<div class="apprenticeship-image-container">
					<div class="apprenticeship-image large"><img src="<?php echo $apprenticeship_image_large[0]; ?>" alt="<?php echo $apprenticeship_image_large_title; ?>"/></div>
					<!--/.apprenticeship-image.large -->
					<div class="apprenticeship-image small">
						<img src="<?php echo $apprenticeship_image_small[0]; ?>" alt="<?php echo $apprenticeship_image_small_title; ?>"/>
					</div>
					<!--/.apprenticeship-image.small -->
				</div>
				<!-- /.apprenticeship-image-container -->
			</div>
			<!-- /.col -->
		
		</div></div>
		<!-- /.wrapper .row -->
			
	</section>
	<!-- /.apprenticeship -->
		
	<section class="section workshops">
			
		<div class="wrapper"><div class="row">
			
			<div class="col-md-6 col-md-offset-1">
				
				<div class="content workshops">
					<h2 class="headline headline--section-title"><?php echo $workshop_headline; ?></h2>
					<div class="details rte"><?php echo $workshop_details; ?></div>
				</div>
				<!-- /.content.workshops -->
			
			</div>
			<!-- /.col -->
						
			<div class="col-md-10 col-md-offset-1">
									
					<?php if ( have_rows("workshop-repeater") ) : ?>
						<ul id="workshop-list">
						<?php while ( have_rows("workshop-repeater") ): the_row(); ?>
						
							<?php 
								// ACF Sub Fields 
								$name = get_sub_field("name");
								$details = get_sub_field("details");
								$attachment_id = get_sub_field("image");
								$image = wp_get_attachment_image_src( $attachment_id, "full" );
								$image_title = get_the_title(  $attachment_id  );
								$url = get_sub_field("url");
								$comingSoon = get_sub_field("coming-soon");
							?>
							
							<li class="workshop-list-item">
								<div class="row inner">
									
									<div class="col-md-5">
										<div class="workshop-image">
											<img src="<?php echo $image[0]; ?>" alt="<?php echo $image_title; ?>" />
										</div>
										<!-- /.workshop-image -->
									</div>
									<!-- /.col -->
									
									<div class="col-md-7">
										<div class="content workshop">
											<h2 class="headline headline--section-title headline--workshop-title"><?php echo $name; ?></h2>
											<div class="details rte"><?php echo $details; ?></div>
																					
											<?php 
												if ( $comingSoon ) :
													$href = "mailto:academy@barberandco.ca?subject=Workshop%20Course%20Request%20:%20" . $name;
													$buttonState = "coming-soon";
													$buttonCta = "Request Course";
												else :
													$href = $url;
													$buttonState = "book-now";
													$buttonCta = "Book Now";
												endif; 
											?>
																					
											<a class="button button--inline button--solid-fill button--<?php echo $buttonState; ?>" href="<?php echo $href; ?>"><?php echo $buttonCta; ?></a>
																					
										</div>
										<!-- /.content.workshop -->
									</div>
									<!-- /.col -->
								
								</div>
							</li>
							<!-- /.workshop-list-item -->
							
						<?php endwhile; ?>
						</ul>
						<!-- /#workshop-list -->
					<?php endif; ?>
					
			</div>
						
		</div></div>
		<!-- /.wrapper .row -->
			
	</section>
	<!-- /.apprenticeship -->

</div>
<!-- /#academy -->

<?php get_footer(); ?>