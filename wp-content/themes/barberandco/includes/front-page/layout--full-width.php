<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Filename: layout--full-width.php
*
*/

// Theme Vars
$home = Daymarker( 'home_url' );
$shopify_url = Daymarker( 'shopify_url' );
$template_dir = Daymarker( 'template_dir' );
$is_production = Daymarker( 'production' );
$is_maintenance = Daymarker( 'maintenance' );
$theme_classes = Daymarker( 'theme_classes' );

// Default Vars
$content_type = false;
$message_obj = array();

// Conditional Vars
if ( get_sub_field( 'content_type' ) ) {
	$content_type = get_sub_field( 'content_type' );
	$message_obj['colour_theme'] = get_sub_field( 'colour_theme' );
	$message_obj['message'] = get_sub_field( 'message' );
	$message_obj['cta'] = get_sub_field( 'cta' );
	$message_obj['link'] = get_sub_field( 'link' );
	$message_obj['pos_hor'] = get_sub_field( 'pos_hor' );
	$message_obj['pos_vert'] = get_sub_field( 'pos_vert' );
	
	switch ( $content_type ) {
		case 'background_images':	
			include( locate_template( './includes/front-page/content--background-images.php' ) );						
			break;
		case 'background_video':
			include( locate_template( './includes/front-page/content--background-video.php' ) );
			break;
		default:
			break;
	}
}

?>