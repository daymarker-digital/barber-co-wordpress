<?php

//////////////////////////////////////////////////////////
////  ACF | Master Options
//////////////////////////////////////////////////////////

if ( function_exists('acf_add_options_page') ) {
			
	acf_add_options_page(
		array(
			'page_title' => 'Full Site Options',
			'menu_title' => 'Full Site Options',
			'menu_slug' => 'full-site-options',
			'capability' => 'edit_posts',
			'parent_slug' => '',
			'position' => false,
			'icon_url' => false,
			//'redirect' => false ( un-commenting this will make this option page the default, top level page
		)
	);
	
	acf_add_options_sub_page(
		array(
			'page_title' => 'General',
			'menu_title' => 'General',
			'menu_slug' => 'full-site-options-general',
			'capability' => 'edit_posts',
			'parent_slug' => 'full-site-options',
			'position' => false,
			'icon_url' => false
		)
	);
	
	acf_add_options_sub_page(
		array(
			'page_title' => 'Header',
			'menu_title' => 'Header',
			'menu_slug' => 'full-site-options-header',
			'capability' => 'edit_posts',
			'parent_slug' => 'full-site-options',
			'position' => false,
			'icon_url' => false
		)
	);
	
	acf_add_options_sub_page(
		array(
			'page_title' => 'Footer',
			'menu_title' => 'Footer',
			'menu_slug' => 'full-site-options-footer',
			'capability' => 'edit_posts',
			'parent_slug' => 'full-site-options',
			'position' => false,
			'icon_url' => false
		)
	);
	
	acf_add_options_sub_page(
		array(
			'page_title' => 'Navigation & Menus',
			'menu_title' => 'Navigation & Menus',
			'menu_slug' => 'full-site-options-navigation',
			'capability' => 'edit_posts',
			'parent_slug' => 'full-site-options',
			'position' => false,
			'icon_url' => false
		)
	);
	
	acf_add_options_sub_page(
		array(
			'page_title' => 'Push Content',
			'menu_title' => 'Push Content',
			'menu_slug' => 'full-site-options-push-content',
			'capability' => 'edit_posts',
			'parent_slug' => 'full-site-options',
			'position' => false,
			'icon_url' => false
		)
	);
		
	acf_add_options_sub_page(
		array(
			'page_title' => 'Company Info',
			'menu_title' => 'Company',
			'menu_slug' => 'full-site-options-company',
			'capability' => 'edit_posts',
			'parent_slug' => 'full-site-options',
			'position' => false,
			'icon_url' => false
		)
	);
		
	acf_add_options_sub_page(
		array(
			'page_title' => 'Maintenance & Debugging',
			'menu_title' => 'Maintenance',
			'menu_slug' => 'full-site-options-maintenance',
			'capability' => 'edit_posts',
			'parent_slug' => 'full-site-options',
			'position' => false,
			'icon_url' => false
		)
	);
				 	    	    
} // if function exists

//////////////////////////////////////////////////////////
////  ACF | Gravity Forms 
//////////////////////////////////////////////////////////

add_action('acf/register_fields', 'my_register_fields');

function my_register_fields() {
	    
    include_once('Gravity-Forms-ACF-Field-master/acf-gravity_forms.php');

}

add_filter( 'gform_submit_button', '__return_false' );

?>