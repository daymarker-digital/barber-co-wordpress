<?php
	
/*
*		
*	Theme: Barber & Co. WordPress Theme
*	Filename: footer.php
*
*/

// Theme Vars
$home = Daymarker( 'home_url' );
$shopify_url = Daymarker( 'shopify_url' );
$template_dir = Daymarker( 'template_dir' );
$is_production = Daymarker( 'production' );
$is_maintenance = Daymarker( 'maintenance' );
$theme_classes = Daymarker( 'theme_classes' );

// Page Vars
$current_page_id = get_the_ID();

$old_form_action = '//donnellygroup.us12.list-manage.com/subscribe/post?u=f9ed1f1423ba352d5eaef6fa7&amp;id=5cb0f8426c';
$new_form_action = 'https://donnellygroup.us20.list-manage.com/subscribe/post?u=49fe23c351b62ac3c03aa99f5&amp;id=e05efec499';

?>

<footer class="<?php echo $theme_classes; ?>">
	
	<div class="footer__main">
	
		<div class="wrapper">
			<div class="row">
			
				<div class="col-xs-9 col-md-3">
				
					<div class="footer__newsletter">
						
						<form action="<?= $new_form_action; ?>" method="post" target="_blank" novalidate="">
							<h2>Get Wishes</h2>
							<fieldset>
								<input class="input input--email" type="email" value="" name="EMAIL" placeholder="I wish I was a little bit taller...">
								<div style="position: absolute; left: -5000px;" aria-hidden="true">
									<input type="text" name="b_f9ed1f1423ba352d5eaef6fa7_5cb0f8426c" tabindex="-1" value="">
								</div>
							</fieldset>
							<fieldset>
								<input type="submit" name="subscribe" value="Subscribe +">
							</fieldset>
						</form>
						
					</div>
					<!-- /.footer__newsletter -->
						
					<div class="footer__brand">
						<a href='<?php echo $home; ?>'>
							<img src='<?php echo $home; ?>/wp-content/themes/barberandco/img/brand/BARBER-web-content-logo-symbol-beige.svg' alt="Barber &amp; Co. Logo">
						</a>
					</div>
					
				</div>
				<!-- /.col -->
			
				<div class="col-xs-12 col-sm-9 col-md-7 col-md-offset-1">
  				
  				<?php 
    				
    				if ( have_rows( 'footer_navigation', 'options' ) ) {
      				      				
      				echo '<nav class="menu menu--footer menu--main">';
      				
        				while ( have_rows( 'footer_navigation', 'options' ) ) {
          				
          				// init data
          				the_row();
          				
          				// default data
          				$title = $menu = false;
          				
          				// get data
          				if ( get_sub_field( 'title' ) ) {
            				$title = get_sub_field( 'title' );            				
          				}
          				
          				echo '<ul class="menu__list menu__list--footer">';
          				
            				if ( $title ) {
              				echo '<li class="menu__item menu__item--footer menu__item--heading">' . $title . '</li>';
            				}
            				
            				if ( have_rows( 'menu' ) ) {
              				while ( have_rows( 'menu' ) ) {
              				
              				  // init data
                        the_row();
                        
                        // default data
                        $title = $type = false;
                        $link_id = $link = $link_active = $link_target = false;
                                                
                        // get data
                        if ( get_sub_field( 'title' ) ) {
                          $title = get_sub_field( 'title' );
                        }
                        if ( get_sub_field( 'type' ) ) {
                          
                          $type = get_sub_field( 'type' );
                          
                          switch ( $type ) {
                          	case 'internal':	
                          		if ( get_sub_field( 'link_internal' ) ) {
                            		$link_id = get_sub_field( 'link_internal' );
                            		$link_active = ( $link_id === $current_page_id );
                            		$link = get_the_permalink( $link_id );
                          		}				
                          		break;
                          	case 'external':	
                          		if ( get_sub_field( 'link_external' ) ) {
                            		$link = get_sub_field( 'link_external' );
                            		$link_target = true;
                          		}					
                          		break;	
                          	case 'shopify':	
                          		if ( get_sub_field( 'link_shopify' ) ) {
                            		$link = $shopify_url . get_sub_field( 'link_shopify' );
                          		}					
                          		break;
                          }
                          
                        }
                        
                        if ( $link && $title ) {
                          echo '<li class="menu__item menu__item--footer ' . ( $link_active ? 'active' : '' ) . '">';
                            echo '<a href="' . $link . '" ' . ( $link_target ? 'target="_blank"' : '' ) . '>' . $title . '</a>';
                          echo '</li>';
                        }
                        
                      }
            				}
                  
                  echo '</ul>';
                  echo '<!-- /.menu__list--info -->';
          				
        				}
      				
      				echo '</nav>';
      				              
            }
            
          ?>
					
				</div>
				<!-- /.col -->
		
			</div>
			<!-- /.row -->
		</div>
		<!-- /.wrapper -->
	
	</div>
	<!-- /.footer__main -->
	
	<div class="footer__legal">
	
		<div class="wrapper"><div class="row"><div class="col-xs-12">
				
			<div class="footer__hr"></div>
			
			<?php 
    				
				if ( have_rows( 'footer_legal', 'options' ) ) {
  				  				
  				echo '<nav class="menu menu--footer menu--legal">';
  				  echo '<ul class="menu__list menu__list--footer menu__list--legal">';
  				
      				while ( have_rows( 'footer_legal', 'options' ) ) {
        				
        				// init data
        				the_row();
        				
        				// default data
                $title = $type = false;
                $link_id = $link = $link_active = $link_target = false;
                
                // get data
                if ( get_sub_field( 'title' ) ) {
                  $title = get_sub_field( 'title' );
                }
                
                if ( get_sub_field( 'type' ) ) {
                  
                  $type = get_sub_field( 'type' );
                  
                  switch ( $type ) {
                  	case 'internal':	
                  		if ( get_sub_field( 'link_internal' ) ) {
                    		$link_id = get_sub_field( 'link_internal' );
                    		$link_active = ( $link_id === $current_page_id );
                    		$link = get_the_permalink( $link_id );
                  		}				
                  		break;
                  	case 'external':	
                  		if ( get_sub_field( 'link_external' ) ) {
                    		$link = get_sub_field( 'link_external' );
                    		$link_target = true;
                  		}					
                  		break;	
                  	case 'shopify':	
                  		if ( get_sub_field( 'link_shopify' ) ) {
                    		$link = $shopify_url . get_sub_field( 'link_shopify' );
                  		}					
                  		break;
                  }
                  
                }
                
                if ( $link && $title ) {
                  echo '<li class="menu__item menu__item--footer ' . ( $link_active ? 'active' : '' ) . '">';
                    echo '<a href="' . $link . '" ' . ( $link_target ? 'target="_blank"' : '' ) . '>' . $title . '</a>';
                  echo '</li>';
                }
                                
      				}
  				
            echo '</ul>';
  				echo '</nav>';
  				              
        }
            
      ?>
				
			<p class="footer__copyright">&copy; <?php echo date('Y'); ?> Barber &amp; Co</p>
				
		</div></div></div>
		<!-- /.wrapper .row .col -->

	</div>
	<!-- /.footer__legal -->
	
</footer>

</div>
<!-- /#site-container -->
	
<?php wp_footer(); ?>

</body>

</html>