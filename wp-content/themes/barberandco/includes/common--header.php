<?php

/*
*
*	Theme: Barber & Co. WordPress Theme
*	Filename: common--push-content.php
*
*/

// Theme Vars
$home = Daymarker( 'home_url' );
$shopify_url = Daymarker( 'shopify_url' );
$template_dir = Daymarker( 'template_dir' );
$is_production = Daymarker( 'production' );
$is_maintenance = Daymarker( 'maintenance' );
$theme_classes = Daymarker( 'theme_classes' );

// Page Vars
$current_page_id = get_the_ID();

?>

<header class="<?php echo $theme_classes; ?> sticky offset-theme light">

	<div class="header__main">

		<div class="push-content-trigger push-content-trigger--burger-menu push-content-trigger--mobile" id="push-content-trigger--burger-menu">
			<img class="offset-theme-icon dark" src="//cdn.shopify.com/s/files/1/2215/4093/t/6/assets/BARBER--ui-icon--burger--dark-grey.svg?671" alt="Main Menu Trigger">
			<img class="offset-theme-icon light" src="//cdn.shopify.com/s/files/1/2215/4093/t/6/assets/BARBER--ui-icon--burger--beige.svg?671" alt="Main Menu Trigger">
		</div>
		<!-- /.header__push-content-trigger--left -->

		<div class="header-cta header-cta--shop push-content-trigger push-content-trigger--cart-summary desktop-only">
  		<span class="copy offset-theme-color">
  		  <a href="<?php echo $shopify_url; ?>/collections/all">Cart</a>
  		</span>
		</div>
		<!-- /.header__push-content-trigger--right -->

		<div class="header-cta header-cta--book push-content-trigger push-content-trigger--book-now">
			<span class="copy offset-theme-color">Book</span>
		</div>
		<!-- /.header__push-content-trigger--right -->

		<div class="header__brand">
			<a href="<?php echo $home; ?>">
	          	<img class="offset-theme-icon dark" src="//cdn.shopify.com/s/files/1/2215/4093/t/6/assets/BARBER--brand--wordmark--dark-grey.svg?671" alt="Beige Barber &amp; Co. Logo">
				<img class="offset-theme-icon light" src="//cdn.shopify.com/s/files/1/2215/4093/t/6/assets/BARBER--brand--wordmark--beige.svg?671" alt="Dark Grey Barber &amp; Co. Logo">
			</a>
		</div>
		<!-- /.header__brand -->

		<div class="header__navigation">

			<nav class="navigation navigation--main">
				<ul>

  				<?php

    				if ( have_rows( 'header_navigation', 'options' ) ) {
              while ( have_rows( 'header_navigation', 'options' ) ) {

                // init data
                the_row();

                // default data
                $title = $type = false;
                $link_id = $link = $link_active = $link_target = false;

                // get data
                if ( get_sub_field( 'title' ) ) {
                  $title = get_sub_field( 'title' );
                  if ( get_sub_field( 'type' ) ) {
                    $type = get_sub_field( 'type' );
                    switch ( $type ) {
                    	case 'internal':
                    		if ( get_sub_field( 'link_internal' ) ) {
                      		$link_id = get_sub_field( 'link_internal' );
                      		$link_active = ( $link_id === $current_page_id );
                      		$link = get_the_permalink( $link_id );
                    		}
                    		break;
                    	case 'external':
                    		if ( get_sub_field( 'link_external' ) ) {
                      		$link = get_sub_field( 'link_external' );
                      		$link_target = true;
                    		}
                    		break;
                    	case 'shopify':
                    		if ( get_sub_field( 'link_shopify' ) ) {
                      		$link = $shopify_url . get_sub_field( 'link_shopify' );
                    		}
                    		break;
                    	case 'no-link':
                    		break;
                    }

                    echo '<li class="navigation-item navigation-item--main navigation-item--desktop offset-theme-color">';

                      if ( 'no-link' === $type ) {
                        echo '<span class="abc">' . $title . '</span>';
                      } else {
                        echo '<a class="abc" href="' . $link . '" ' . ( $link_target ? 'target="_blank"' : '' ) . '>' . $title . '</a>';
                      }

                      if ( have_rows( 'sub_nav' ) || have_rows( 'cta_nav' ) ) {

                        echo '<nav class="navigation navigation--sub">';
                          echo '<div class="wrapper"><div class="row"><div class="col-md-12">';

                          //////////////////////////////////////////////////////////
                          ////  Sub Navigation
                          //////////////////////////////////////////////////////////

                          if ( have_rows( 'sub_nav' ) ) {
                            echo '<ul>';
                            while ( have_rows( 'sub_nav' ) ) {

                              // init data
                              the_row();

                              // default data
                              $title = $type = false;
                              $link_id = $link = $link_active = $link_target = false;

                              // get data
                              if ( get_sub_field( 'title' ) ) {
                                $title = get_sub_field( 'title' );
                                if ( get_sub_field( 'type' ) ) {
                                  $type = get_sub_field( 'type' );
                                  switch ( $type ) {
                                  	case 'internal':
                                  		if ( get_sub_field( 'link_internal' ) ) {
                                    		$link_id = get_sub_field( 'link_internal' );
                                    		$link_active = ( $link_id === $current_page_id );
                                    		$link = get_the_permalink( $link_id );
                                  		}
                                  		break;
                                  	case 'external':
                                  		if ( get_sub_field( 'link_external' ) ) {
                                    		$link = get_sub_field( 'link_external' );
                                    		$link_target = true;
                                  		}
                                  		break;
                                  	case 'shopify':
                                  		if ( get_sub_field( 'link_shopify' ) ) {
                                    		$link = $shopify_url . get_sub_field( 'link_shopify' );
                                  		}
                                  		break;
                                  	case 'no-link':
                                  		break;
                                  }

                                  if ( 'no-link' === $type ) {
                                    echo '<li class="navigation-item navigation-item--sub navigation-item--heading">' . $title . '</li>';
                                  } else {
                                    echo '<li class="navigation-item navigation-item--sub ' . ( $link_active ? 'active' : '' ) . '">';
                                      echo '<a href="' . $link . '" ' . ( $link_target ? 'target="_blank"' : '' ) . '>' . $title . '</a>';
                                    echo '</li>';
                                  }

                                }
                              }

                            }
                            echo '</ul>';
                          }

                          //////////////////////////////////////////////////////////
                          ////  Call to Action
                          //////////////////////////////////////////////////////////

                          if ( have_rows( 'cta_nav' ) ) {
                            echo '<ul class="call-to-actions">';
                            while ( have_rows( 'cta_nav' ) ) {

                              // init data
                              the_row();

                              // default data
                              $heading = $subheading = false;
                              $image = $image_xs = false;
                              $link_id = $link = $type = false;

                              // get data
                              if ( get_sub_field('heading') ) {
                                $heading = get_sub_field('heading');
                              }
                              if ( get_sub_field('subheading') ) {
                                $subheading = get_sub_field('subheading');
                              }
                              if ( get_sub_field('type') ) {
                                $type = get_sub_field('type');
                                switch ( $type ) {
                                	case 'internal':
                                	    if ( get_sub_field( 'link_internal' ) ) {
                                    		$link_id = get_sub_field( 'link_internal' );
                                    		$link = get_the_permalink( $link_id );
                                  		}
                                		break;
                                	case 'shopify':
                                	    if ( get_sub_field( 'link_shopify' ) ) {
                                    		$link = $shopify_url . get_sub_field( 'link_shopify' );
                                  		}
                                		break;
                                	default:
                                		break;
                                }
                              }
                              if ( get_sub_field('image') ) {
                                $image = get_sub_field('image');
                              }

                              echo '<li class="call-to-actions__item">';
                                echo '<a href="' . $link . '">';

                                  if ( $image ) {
                                    echo '<div class="call-to-actions__image lazyload-container lazyload-container--background-image">';
                                      echo '<div
                                        class="lazyload lazyload--background-image"
                                        data-bg="' . $image['sizes']['tablet-small'] . '"
                                        style="background-image: url(' . $image['sizes']['lazyload-tiny'] . ');">
                                        </div>';
                                    echo '</div>';
            			                }

                                  if ( $heading ) {
                                    echo '<span class="call-to-actions__headline">' . $heading . '</span>';
                                  }
                                  if ( $subheading ) {
                                    echo '<span class="call-to-actions__headline call-to-actions__headline--sub">' . $subheading . '</span>';
                                  }

                                echo '</a>';
                              echo '</li>';

                            }
                            echo '</ul>';
                            echo '<!-- /.call-to-actions -->';
                          }

                          echo '</div></div></div>';
                        echo '</nav>';

                      }

                    echo '</li>';
                    echo '<!-- /.navigation-item -->';

                  }
                }

              }
            }

    				/*
      				<li class="navigation-item navigation-item--main navigation-item--desktop offset-theme-color">
              <a class="abc" href="<?php echo $shopify_url; ?>/collections/all/">Products</a>
                <nav class="navigation navigation--sub">
                  <div class="wrapper"><div class="row"><div class="col-md-12">

    								<ul>
    										<li class="navigation-item navigation-item--sub"><a href="<?php echo $shopify_url; ?>/collections/hair">Hair</a></li>
    										<li class="navigation-item navigation-item--sub"><a href="<?php echo $shopify_url; ?>/collections/shave">Shave</a></li>
    										<li class="navigation-item navigation-item--sub"><a href="<?php echo $shopify_url; ?>/collections/beard">Beard</a></li>
    										<li class="navigation-item navigation-item--sub"><a href="<?php echo $shopify_url; ?>/collections/gifts">Gifts</a></li>
    										<li class="navigation-item navigation-item--sub"><a href="<?php echo $home; ?>/lab/">Lab</a></li>
    										<li class="navigation-item navigation-item--sub"><a href="<?php echo $shopify_url; ?>/collections/grooming">All</a></li>
    								</ul>




                    <ul class="call-to-actions">
                      <li class="call-to-actions__item">
                        <a href="<?php echo $shopify_url; ?>/products/classic-pomade">
                          <div class="call-to-actions__image lazyload-container lazyload-container--background-image">
                            <div class="lazyload lazyload--background-image" data-bg="//cdn.shopify.com/s/files/1/2215/4093/t/6/assets/BARBER-nav-cta--product--classic-pomade.jpg?671"></div>
    			                </div>
                          <span class="call-to-actions__headline">Classic Pomade</span>
                          <span class="call-to-actions__headline call-to-actions__headline--sub">Medium hold for high-shine style</span>
                        </a>
                      </li>
                      <!-- /.call-to-actions__item -->
                      <li class="call-to-actions__item">
                        <a href="<?php echo $shopify_url; ?>/products/shave-oil">
                          <div class="call-to-actions__image lazyload-container lazyload-container--background-image">
                            <div class="lazyload lazyload--background-image" data-bg="//cdn.shopify.com/s/files/1/2215/4093/t/6/assets/BARBER-nav-cta--product--shave-oil.jpg?671"></div>
    			                </div>
                          <span class="call-to-actions__headline">Shave Oil</span>
                          <span class="call-to-actions__headline call-to-actions__headline--sub">Hydrate, calm + heal skin</span>
                        </a>
                      </li>
                    </ul>






                  </div></div></div>
  	              <!-- /.wrapper .row .col -->
                </nav>
                <!-- /.navigation--sub -->
  					</li>
            <!-- /.navigation-item--main -->

  					<li class="navigation-item navigation-item--main navigation-item--desktop offset-theme-color">
  						<span class="abc">Shops</span>
  						<nav class="navigation navigation--sub">
                <div class="wrapper"><div class="row"><div class="col-md-12">

                  <ul>
                    <li class="navigation-item navigation-item--sub navigation-item--heading">Vancouver</li>
                    <li class="navigation-item navigation-item--sub"><a href="<?php echo $home; ?>/barbershop/gastown/">Gastown</a></li>
                    <li class="navigation-item navigation-item--sub"><a href="<?php echo $home; ?>/barbershop/main-street/">Main Street</a></li>
                    <li class="navigation-item navigation-item--sub"><a href="<?php echo $home; ?>/barbershop/financial-district/">Financial District</a></li>
                    <li class="navigation-item navigation-item--sub"><a href="<?php echo $home; ?>/barbershop/yaletown/">Yaletown</a></li>
  									<li class="navigation-item navigation-item--sub navigation-item--heading">Toronto</li>
                    <li class="navigation-item navigation-item--sub"><a href="<?php echo $home; ?>/barbershop/bay-street/">Bay Street</a></li>
                    <li class="navigation-item navigation-item--sub"><a href="<?php echo $home; ?>/barbershop/ossington/">Ossington</a></li>
  								</ul>

                  <ul class="call-to-actions">
                    <li class="call-to-actions__item">
                      <a href="<?php echo $home; ?>/barbershop/ossington">
                        <div class="call-to-actions__image lazyload-container lazyload-container--background-image">
  												<div class="lazyload lazyload--background-image" data-bg="//cdn.shopify.com/s/files/1/2215/4093/t/6/assets/BARBER-nav-cta--location--ossington.jpg?671"></div>
                        </div>
                        <span class="call-to-actions__headline">Gift Shop</span>
                        <span class="call-to-actions__headline call-to-actions__headline--sub">Ossington Avenue, Toronto</span>
                      </a>
                    </li>
                    <!-- /.call-to-actions__item -->
                    <li class="call-to-actions__item">
                      <a href="<?php echo $home; ?>/barbershop/financial-district">
                        <div class="call-to-actions__image lazyload-container lazyload-container--background-image">
                          <div class="lazyload lazyload--background-image" data-bg="//cdn.shopify.com/s/files/1/2215/4093/t/6/assets/BARBER-nav-cta--location--financial-district.jpg?671"></div>
                        </div>
                        <span class="call-to-actions__headline">Financial District Barbershop</span>
                        <span class="call-to-actions__headline call-to-actions__headline--sub">Dunsmuir Street, Vancouver</span>
                      </a>
                    </li>
                    <!-- /.call-to-actions__item -->
                  </ul>

                </div></div></div>
                <!-- /.wrapper .row .col -->
              </nav>
              <!-- /.navigation--sub -->
            </li>
  					<!-- /.navigation-item--main -->

  					<li class="navigation-item navigation-item--main navigation-item--desktop offset-theme-color">
  						<span class="abc">Bars</span>
  						<nav class="navigation navigation--sub">
                <div class="wrapper"><div class="row"><div class="col-md-12">

                  <ul>
                    <li class="navigation-item navigation-item--sub navigation-item--heading">Toronto</li>
                    <li class="navigation-item navigation-item--sub"><a href="<?php echo $home; ?>/bar/gift-shop/">Gift Shop</a></li>
  								</ul>

                  <ul class="call-to-actions">
                    <li class="call-to-actions__item">
                      <a href="<?php echo $home; ?>/bar/gift-shop/">
                        <div class="call-to-actions__image lazyload-container lazyload-container--background-image">
  											  <div class="lazyload lazyload--background-image" data-bg="http://barber-wp/wp-content/uploads/2020/01/DSCF6441-resized.jpg"></div>
                        </div>
                        <span class="call-to-actions__headline">Ossington Bar</span>
                        <span class="call-to-actions__headline call-to-actions__headline--sub">Ossington Avenue, Toronto</span>
                      </a>
                    </li>
                  </ul>

                </div></div></div>
                <!-- /.wrapper .row .col -->
              </nav>
              <!-- /.navigation--sub -->
            </li>
  					<!-- /.navigation-item--main -->
  					*/

  				?>

					<li class="navigation-item navigation-item--main mobile-only offset-theme-color"><a href="<?php echo $shopify_url; ?>/collections/all">Cart</a></li>
					<!-- /.navigation-item--main -->

				</ul>
			</nav>
			<!-- /.navigation--main -->

		</div>
		<!-- /.header__navigation -->

	</div>
	<!-- /.header__main -->

</header>
<!-- header.sticky -->
