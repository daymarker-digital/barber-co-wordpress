<?php

//////////////////////////////////////////////////////////
////  Enqueue Script & Style 
//////////////////////////////////////////////////////////

function daymarker_enqueue_scripts () {
	
    if ( $GLOBALS['pagenow'] != 'wp-login.php' && !is_admin() ) {
	    	    
	    // Script & Styles Variables
	    $google_maps_api_key = false;
	    $page_with_maps = false;
	    
	    if ( is_singular('event') ) {
			$page_with_maps = true;  
	    }
	    
	    // removes default wp version of jquery
	    wp_deregister_script( 'jquery-core' );
		wp_deregister_script( 'wp-embed' );
	   	    
	    // Main Styles
	    wp_register_style('css-style', get_template_directory_uri() . '/style.css', array(), filemtime( get_stylesheet_directory() . '/style.css' ), 'all' );	
		wp_enqueue_style('css-style');
		
		// Site JS
    	wp_register_script('jquery-core', get_template_directory_uri() . '/js/min/jQuery--1.12.4.min.js', array(), filemtime( get_template_directory() . '/js/min/jQuery--1.12.4.min.js' ), false );
		wp_register_script('js-daymarker', get_template_directory_uri() . '/js/min/Daymarker.min.js', array('jquery-core'), filemtime( get_template_directory() . '/js/min/Daymarker.min.js' ), true );
		
        // Enqueue Scripts
		wp_enqueue_script('jquery-core');
		wp_enqueue_script('js-daymarker');
				
		if ( $google_maps_api_key && $page_with_maps ) {
			// IMPORTANT!!! NEEDS TO BE ENQUEUED AFTER 'DAYMARKER.JS'
			wp_register_script('js-google-maps', 'https://maps.googleapis.com/maps/api/js?key=' . $google_maps_api_key . '&callback=GoogleMaps.init', array('js-daymarker'), '', true );
			wp_enqueue_script('js-google-maps');
		}
                
    }
    
}

add_action( 'wp_enqueue_scripts', 'daymarker_enqueue_scripts' );

//////////////////////////////////////////////////////////
////  Conditionally Enqueue Script & Style 
//////////////////////////////////////////////////////////

function daymarker_enqueue_scripts_conditionally () {
	
    if ( is_user_logged_in() || is_admin() ) {
	    // wp_register_style('css-wp-admin-dashboard', get_template_directory_uri() . '/css/wp-admin-dashboard.css', array(), filemtime( get_stylesheet_directory() . '/css/wp-admin-dashboard.css' ), 'all' );	
		// wp_enqueue_style('css-wp-admin-dashboard');
    }
    
}

add_action( 'init', 'daymarker_enqueue_scripts_conditionally' );

?>