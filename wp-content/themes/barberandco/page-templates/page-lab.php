<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Template Name: Page [ Lab ]
*	Filename: page-lab.php
*
*/

get_header();
 
// ACF Vars
$lab_headline = get_field("lab-headline");
$lab_details = get_field("lab-details");

$attachment_id = get_field("lab-image-large");	
$lab_image_large = wp_get_attachment_image_src( $attachment_id, "full" );
$lab_image_large_title = get_the_title(  $attachment_id  );

$attachment_id = get_field("lab-image-small");	
$lab_image_small = wp_get_attachment_image_src( $attachment_id, "full" );
$lab_image_smal_title = get_the_title(  $attachment_id  );

$suggested_products_headline = get_field("suggested-products-headline");

?>

<div id="lab" class="page page--lab" role="main">
	
	<?php if ( get_field( "cover-image" ) ) : ?>
		<!-- Cover Image -->
		<?php include( locate_template( "./includes/common--cover.php" ) ); ?>
		<!-- End of Cover Image -->
	<?php endif; ?>
	
	<section class="section section--lab">
		
		<div class="lab__header">
			<div class="wrapper"><div class="row"><div class="col-md-7 col-md-offset-3">
			
				<?php if ( get_field( "lab-headline" ) ) : ?>
					<h2 class="headline headline--section-title"><?php the_field( "lab-headline" ); ?></h2>
				<?php endif; ?>
				
				<?php if ( get_field( "lab-details" ) ) : ?>
					<div class="message message--rte"><?php the_field( "lab-details" ); ?></div>
				<?php endif; ?>
						
			</div></div></div>
			<!-- /.wrapper .row .col -->
	
		</div>
		<!-- /.lab__header -->
			
		<div class="lab__main">
				
			<div class="wrapper"><div class="row"><div class="col-md-10 col-md-offset-1">
								
				<?php if ( get_field( "lab-image-large" ) || get_field( "lab-image-large" ) ) : ?>	
				
					<div class="lab-images">
						
						<?php 
							$image = $image_src = $image_alt = false;
							if ( get_field( "lab-image-large" ) ) {
								$image = get_field( "lab-image-large" );
								$image_src = $image['url'];
								$image_alt = $image['alt'];
							}
						?>
						<div class="images image--large lazyload-container lazyload-container--image">
							<img class="lazyload lazyload--image" data-src="<?php echo $image_src; ?>" alt="<?php echo $image_alt; ?>" />
						</div>
						<!-- /.image--large -->
						
						<?php 
							$image = $image_src = $image_alt = false;
							if ( get_field( "lab-image-small" ) ) {
								$image = get_field( "lab-image-small" );
								$image_src = $image['url'];
								$image_alt = $image['alt'];
							}
						?>
						<div class="images image--small lazyload-container lazyload-container--image">
							<img class="lazyload lazyload--image" data-src="<?php echo $image_src; ?>" alt="<?php echo $image_alt; ?>" />
						</div>
						<!-- /.image--small -->
									
					</div>
					<!-- /.lab-images -->
					
				<?php endif; ?>

			</div></div></div>
			<!-- /.wrapper .row .col -->
							
		</div>
		<!-- /.lab__main -->		
				
	</section>
	<!-- /.section--lab -->
		
	<section class="section section--products">
		
		<div class="products__header">
			<div class="wrapper"><div class="row"><div class="col-sm-10 col-sm-offset-1">
			
				<?php if ( get_field( "suggested-products-headline" ) ) : ?>
					<h2 class="headline headline--section-title"><?php the_field( "suggested-products-headline"  ); ?></h2>
				<?php endif; ?>
				
				<?php if ( get_field( "suggested-products-message" ) ) : ?>
					<div class="message message--rte"><?php the_field( "suggested-products-message" ); ?></div>
				<?php endif; ?>
						
			</div></div></div>
			<!-- /.wrapper .row .col -->
	
		</div>
		<!-- /.products__header -->
									
		
		<div class="products__main">
			<div class="wrapper"><div class="row"><div class="col-xs-12">														
				<?php if ( have_rows("suggested-product-repeater") ) : ?>
				
					<ul class="product-list product-list--lab">
						
						<?php while (  have_rows("suggested-product-repeater")  ) : the_row(); ?>
						
							<?php
								// ACF Sub Fields
								$title = get_sub_field("name");
								$price = get_sub_field("price");
								$url = get_sub_field("link");
								$image = $image_src = $image_alt = false;
								if ( get_sub_field( "image" ) ) {
									$image = get_sub_field( "image" );
									$image_src = $image['url'];
									$image_alt = $image['alt'];
								}	
							?>											
							
							<li class="product-list-item product-list-item--lab">
								
								<a href="<?php echo $url; ?>">
									<div class="product-image lazyload-container lazyload-container--image">
										<img class="lazyload lazyload--image" data-src="<?php echo $image_src; ?>" alt="<?php echo $image_alt; ?>" />
									</div>
									<!-- /.product-image -->
									<h3 class="headline headline--product-title"><?php echo $title; ?></h3>
								</a>
							
							</li>
							<!-- /.suggested-product-item -->
							
						<?php endwhile; ?>
					
					</ul>
					<!-- /.product-list--lab -->
				
				<?php endif; ?>
					
			</div></div></div>
			<!-- /.wrapper .row .col -->
			
		</div>
		<!-- /.products__main -->
									
	</section>
	<!-- /.section--products -->
						
</div>
<!-- /#lab -->

<?php get_footer(); ?>