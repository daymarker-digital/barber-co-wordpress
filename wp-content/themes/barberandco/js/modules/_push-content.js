//////////////////////////////////////////////////////////
////  Push Content
//////////////////////////////////////////////////////////

var PushContent = (function () {
	
	var debugThis = false;
	var info = { name : 'PushContent', version : 1.0 };
	
	//////////////////////////////////////////////////////////
	////  Private Methods
	//////////////////////////////////////////////////////////
		
	var _headerOnClick = function () {
      
		$("header").on("click touch touchmove", ".push-content-trigger", function( event ) {	
		
			event.stopPropagation(); 		
		
			$("header").addClass("push-content-active");	
			$( "#site-container, .push-content, header.sticky" ).removeClass("pushed pushed-left pushed-right");
		
		            	                  
	        if ( $(this).hasClass("push-content-trigger--burger-menu") ) {
				$( "#site-container, .push-content--main-menu, header.sticky" ).toggleClass("pushed pushed-right");		
			}
	      
			if ( $(this).hasClass("push-content-trigger--book-now") ) {
				$( ".push-content--booking-locations" ).toggleClass("pushed pushed-left");
	        }
	            
	        if ( $(this).hasClass("push-content-trigger--cart-summary") ) {
				$( ".push-content--cart-summary" ).toggleClass("pushed pushed-right");	
	        }
            
		});
      
	};
	
	var _documentOnClick = function () {
		
		$( document ).on( "click touchstart", function( event ) {  
	  	
			var eventTarget = event.target;
			var closePushContent = true;
      
			if ( $(event.target).closest('.push-content').length !== 0 || $(event.target).hasClass("add-to-cart") ) {
        		closePushContent = false;          
			} 
      
			if ( closePushContent ) {
				$( "#site-container, .push-content, header.sticky" ).removeClass("pushed pushed-left pushed-right");	
				$("header").removeClass("push-content-active");	
			}
			
		});

	};
	
	//////////////////////////////////////////////////////////
	////  Public Methods
	//////////////////////////////////////////////////////////
			
	var init = function( $params ) {
      
		Debug.log( debugThis, info, 'start' );
      
		_headerOnClick();
		_documentOnClick();
            
		Debug.log( debugThis, info, 'stop' );	
      
	};
	
	//////////////////////////////////////////////////////////
	////  Returned
	//////////////////////////////////////////////////////////
	
	return {
		info : info,
		init : init
	};

})();