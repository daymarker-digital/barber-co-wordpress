<?php

/*
*
*	Theme: Barber & Co. WordPress Theme
*	Filename: common--push-content.php
*
*/

// Theme Vars
$home = Daymarker("home_url");
$shopify_url = Daymarker("shopify_url");
$template_dir = Daymarker("template_dir");
$is_production = Daymarker("production");
$is_maintenance = Daymarker("maintenance");
$theme_classes = Daymarker("theme_classes");

$product_cats = [
	[ "name" => "Hair", "url" => "/collections/hair" ],
	[ "name" => "Shave", "url" => "/collections/shave" ],
	[ "name" => "Beard", "url" => "/collections/beard" ],
	[ "name" => "Gifts", "url" => "/collections/gifts" ],
	[ "name" => "All", "url" => "/collections/grooming" ],
];

?>

<div class="push-content push-content--left push-content--main-menu" id="push-content--left">

	<div class="brand">
		<a href="<?php echo $home; ?>">
			<img src="<?php echo $template_dir; ?>/img/brand/BARBER-web-content-logo-symbol-dark-grey.svg" alt="Barber & Co. Logo" />
		</a>
	</div>

	<nav class="menu menu--push menu--main">

		<ul class="menu__list menu__list--push menu__list--main">

			<li class="menu__item menu__item--collapsible">
				<div class="collapsible">
					<div class="collapsible__trigger" data-toggle="collapse" data-target="#products">
						<span class="collapsible__icon">+</span>
						<span class="collapsible__title">Products</span>
					</div>
					<div class="collapsible__content collapse in" id="products">
						<div class="collapsible__padding">
							<ul class="sub-menu__list sub-menu__list--push sub-menu__list--main">
								<?php foreach ( $product_cats as $cats ) : ?>
									<?php
										$category_name = $cats["name"] ?? "";
										$category_url = isset($cats["url"]) && !empty($cats["url"]) ? $shopify_url . $cats["url"] : "";
									?>
									<?php if ( $category_name && $category_url ) : ?>
										<li class="sub-menu__item sub-menu__item--shopify updated">
											<a href="<?= esc_url($category_url); ?>"><?= esc_html($category_name); ?></a>
										</li>
									<?php endif; ?>
								<?php endforeach; ?>
							</ul>
						</div>
					</div>
				</div>
			</li>

			<li class="menu__item menu__item--collapsible">
				<div class="collapsible">
					<div class="collapsible__trigger" data-toggle="collapse" data-target="#shops">
						<span class="collapsible__icon">+</span>
						<span class="collapsible__title">Shops</span>
					</div>
					<div class="collapsible__content collapse in" id="shops">
						<div class="collapsible__padding">
							<ul class="sub-menu__list sub-menu__list--push sub-menu__list--main">
								<li class="sub-menu__item sub-menu__item--heading">Vancouver</li>
								<li class="sub-menu__item sub-menu__item--internal">
									<a href="<?php echo $home; ?>/barbershop/main-street/">Main Street</a>
								</li>
								<li class="sub-menu__item sub-menu__item--internal">
									<a href="<?php echo $home; ?>/barbershop/yaletown/">Yaletown</a>
								</li>
								<li class="sub-menu__item sub-menu__item--heading">Toronto</li>
								<li class="sub-menu__item sub-menu__item--internal">
									<a href="<?php echo $home; ?>/barbershop/ossington/">Ossington</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</li>

			<li class="menu__item menu__item--internal">
				<a href="<?php echo $home; ?>/lab/">Lab</a>
			</li>

			<li class="menu__item menu__item--internal">
				<a href="<?php echo $home; ?>/about/">About</a>
			</li>

			<li class="menu__item menu__item--internal">
				<a href="<?php echo $home; ?>/contact/">Contact</a>
			</li>

		</ul>

	</nav>

	<div class="hr"></div>

	<nav class="menu menu--push menu--social">
		<ul class="menu__list menu__list--push menu__list--social">
			<li class="menu__item menu__item--external">
				<a href="https://www.instagram.com/barberandco/" target="_blank">Instagram</a>
			</li>
			<li class="menu__item menu__item--external">
				<a href="https://www.facebook.com/barberandcoyaletown" target="_blank">Facebook</a>
			</li>
		</ul>
	</nav>

</div>
