<?php
	
/*
*	
*	Theme: Barber & Co. WordPress Theme
*	Filename: content--background-video.php
*
*/

// Default Vars
$video_fallback = false;
if ( get_sub_field( 'video_fallback' ) ) {
	$video_fallback = get_sub_field( 'video_fallback' );
	$video_fallback = $video_fallback['url'];
}

// message vars
$message_obj = array();
$message_obj['colour_theme'] = get_sub_field( 'colour_theme' );
$message_obj['message'] = get_sub_field( 'message' );
$message_obj['cta'] = get_sub_field( 'cta' );
$message_obj['link'] = get_sub_field( 'link' );
$message_obj['pos_hor'] = get_sub_field( 'pos_hor' );
$message_obj['pos_vert'] = get_sub_field( 'pos_vert' );

// Get Values
if ( have_rows( 'video_sources' ) ) {
	
	echo '<div class="background-video">';
	
	if ( $video_fallback ) {
		echo '<div class="background-video__fallback-image lazyload-container lazyload-container">';
		echo '<div class="lazyload lazyload--background-image" data-bg="' . $video_fallback . '"></div>';
		echo '</div>';	
		echo '<!-- /.background-video__fallback-image -->';
	}
		
	echo '<div class="background-video__video">';
	echo '<video loop autoplay muted>';
				
	while ( have_rows( 'video_sources' ) ) {
						
		// setup row
		the_row();				
							
		// video source vars
		$video = $video_url = $video_type = false;
							
		// check if there is content
		if ( get_sub_field( 'video' ) ) {
			$video = get_sub_field( 'video' );
			$video_url = $video['url'];
			$video_type = $video['mime_type'];
			echo '<source data-src="' . $video_url . '" data-type="' . $video_type . '" />';
		}	
							
	}	
			
	echo '</video>';
	echo '</div>';
	echo '<!-- /.background-video__video -->';	
		
	echo '<div class="background-video__controls">';
	echo '</div>';
	echo '<!-- /.background-video__controls -->';
		
	include( locate_template( './includes/front-page/content--message.php' ) );
				
	echo '</div>';
	echo '<!-- /.background-video -->';
	
} else {
	echo '<!-- No Video Source(s) -->';
}
				
?>

