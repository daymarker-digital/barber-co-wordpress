//////////////////////////////////////////////////////////
////  Instagram Feed
//////////////////////////////////////////////////////////

var InstagramFeed = (function () {
	
	var debugThis = false;
	var info = { name : 'InstagramFeed', version : 1.2 };	
	var accountInfo = {
		userInfo : 'barberandco',
		userID : '507451900',
		clientID : '5859c095582a4fdfa935d2ee6cad8df9',
		accessToken : '507451900.5859c09.a692bce5868041e8ab1e4df67c634101'
	};
	
	//////////////////////////////////////////////////////////
	////  Private Methods
	//////////////////////////////////////////////////////////
		
	var _getFeedData = function ( $feedOptions, $feedCount, $containerEl ) {
		
		if ( true ) {
      
	    	$.ajax({
	            url: 'https://api.instagram.com/v1/users/' + accountInfo.userID + '/media/recent',
				dataType: 'json',
	            type: 'GET',
	            data: { access_token: accountInfo.accessToken, count: $feedCount },
	            success: function( data ){
		            
		            console.log( data );
	                
	                for ( x in data.data ) {
						
						// Get Vars
						var postLink = data.data[x].link;
	                    var imgURL = data.data[x].images.standard_resolution.url;
	                    var caption = data.data[x].caption.text;
	                    var username = data.data[x].user.username;
	                     
	                    switch ( $feedOptions.display ) {
	                        case "background-image":
	                        	$containerEl.find(".background-image a").attr('data-bg','' + imgURL + '');
	                        	break;
	                        case "inline-image":
	                        	$containerEl.find(".inline-image img").attr('data-src','' + imgURL + '');
	                        	break;
	                    }
	                        												
						$containerEl.find(".instagram__post-link").attr('href','' + postLink + '');
						$containerEl.find(".instagram__message").html( caption );
	                                                
	                }
	            },
	            error: function(data){
	            	console.log( data ); // send the error notifications to console
	            }
			});
			
		} else {
			
			console.log( '[ _getFeedData() ] Error: ' );
			Debug.thisVar( [ $feedOptions, $feedCount, $containerEl ] );
			
		}
      
	};

	//////////////////////////////////////////////////////////
	////  Public Methods
	//////////////////////////////////////////////////////////
	
	var frontPageFeed = function () {
			  	
		if ( $("#front-page").length ) { 
			
			var feedCount = 1;	
			var containerEl = $(".section--instagram");
			var feedOptions = JSON.parse( window.wp_localized_script__instafeed_options );
			_getFeedData( feedOptions, feedCount, containerEl );

		} else {
			if ( debugThis ) {
				console.log( '[ frontPageFeed() ] Error: #front-page does not exist' );	
			}
		}
		
	};
			
	var init = function( $params ) {
      
      	Debug.log( debugThis, info, 'start' );
      
      	frontPageFeed();
            
		Debug.log( debugThis, info, 'stop' );	
      
	};
	
	//////////////////////////////////////////////////////////
	////  Returned
	//////////////////////////////////////////////////////////
	
	return {
      	info : info,
		init : init
	};

})();